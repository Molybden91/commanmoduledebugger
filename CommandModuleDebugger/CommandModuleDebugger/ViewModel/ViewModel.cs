﻿using CommandModuleDebugger.Auxiliar;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace CommandModuleDebugger
{
    public class ViewModel : INotifyPropertyChanged
    {
        #region Constants
        public const string RobotStandardIP = "192.168.1.10";
        public const int RobotStandardPort = 7;
        #endregion

        #region ComboBoxesSelection (private)
        private NumericMode _selectedNumericMode;
        private ConnectionMode _selectedConnectionMode;
        #endregion

        #region CheckBoxesSelection (private)
        private bool _isActuatorDistanceSelected;
        private bool _isActuatorCurrentSelected;
        private bool _isActuatorVelocitySelected;
        private bool _isActuatorReboundSelected;
        #endregion

        #region RadioButtonsSelection (private)
        private CameraType _selectedCamera;
        private int _selectedCameraID;
        private CameraBlockType _selectedCameraBlock;
        private ActuatorBlockType _selectedActuator;
        private byte _selectedActuatorsID;
        private MotorBlockType _selectedMotor;
        private byte _selectedMotorID;
        private MagnetBlockType _selectedMagnet;
        private byte _selectedMagnetID;
        private INAGroupType _selectedINAGroup;
        private INAModuleType _selectedINAModule;
        private byte _selectedInaModuleID;
        private string _inaFirstModule;
        private string _inaSecondModule;
        private string _inaThirdModule;
        private string _inaFourthModule;
        private Visibility _inaThirdVisibility;
        private Visibility _inaFourthVisibility;
        #endregion

        #region TextBoxes (private)
        //Main window
        private string _movementVelocity;
        private string _stepDistance;
        //INA
        private string _inaCurrent;
        private string _inaDuration;
        //camera
        private string _cameraPosition;
        private string _lightBrigtness;
        //actuators
        private string _actuatorStepDistance;
        private string _actuatorVelocity;
        private string _actuatorCurrent;
        private string _reboundValue;
        //motors
        private string _leftMotorPosition;
        private string _rightMotorPosition;
        //magnets
        private string _magnetDegreeOfShutdown;
        //movement
        private double _xMovementCoordinate;
        private double _yMovementCoordinate;
        //professional mode
        private string _proId;
        private string _proDlc;
        private string _proData0;
        private string _proData1;
        private string _proData2;
        private string _proData3;
        private string _proData4;
        private string _proData5;
        private string _proData6;
        private string _proData7;
        #endregion

        #region ListBoxes displaying/answers (private)
        private List<string> _generalParameters;
        private Dictionary<string, Dictionary<string, string>> _unitParameters;
        private Dictionary<string, Dictionary<string, string>> StateUnitParameters;
        private List<string> _unitparametersForDisplay;
        private List<string> _enteredCommands;
        private List<string> _currentCommandsList;
        #endregion

        #region Controllers,device (private)
        private CommandControllerGeneral _generalCommandController;
        private DecodingController _decodingController;
        private DecodingController _unitDecodingController;
        private CommandControllerUnit _unitCommandController;
        private EthernetDevice Robot;
        private Thread GeneralThread;
        #endregion

        #region ComboBoxes Binding (public)
        public NumericMode SelectedNumericMode {
            get { return _selectedNumericMode; }
            set {
                _selectedNumericMode = value;
                OnPropertyChanged("SelectedNumericMode");
            }
        }
        public ConnectionMode SelectedConnectionMode {
            get { return _selectedConnectionMode; }
            set {
                _selectedConnectionMode = value;
                OnPropertyChanged("SelectedConnectionMode");
            }
        }
        #endregion

        #region CheckBoxes Binding (public)
        public bool IsActuatorDistanceSelected {
            get { return _isActuatorDistanceSelected; }
            set {
                _isActuatorDistanceSelected = value;
                OnPropertyChanged("IsActuatorDistanceSelected");
            }
        }
        public bool IsActuatorCurrentSelected {
            get { return _isActuatorCurrentSelected; }
            set {
                _isActuatorCurrentSelected = value;
                OnPropertyChanged("IsActuatorCurrentSelected");
            }
        }
        public bool IsActuatorVelocitySelected {
            get { return _isActuatorVelocitySelected; }
            set {
                _isActuatorVelocitySelected = value;
                OnPropertyChanged("IsActuatorVelocitySelected");
            }
        }
        public bool IsActuatorReboundSelected {
            get { return _isActuatorReboundSelected; }
            set {
                _isActuatorReboundSelected = value;
                OnPropertyChanged("IsActuatorReboundSelected");
            }
        }
        #endregion

        #region RadioButtons Binding (public)
        public Visibility INAThirdVisibility {
            get { return _inaThirdVisibility; }
            set {
                _inaThirdVisibility = value;
                OnPropertyChanged("INAThirdVisibility");
            }
        }
        public Visibility INAFourthVisibility
        {
            get { return _inaFourthVisibility; }
            set
            {
                _inaFourthVisibility = value;
                OnPropertyChanged("INAFourthVisibility");
            }
        }
        public string INAFirstModule {
            get { return _inaFirstModule; }
            set {
                _inaFirstModule = value;
                OnPropertyChanged("INAFirstModule");
            }
        }
        public string INASecondModule
        {
            get { return _inaSecondModule; }
            set
            {
                _inaSecondModule = value;
                OnPropertyChanged("INASecondModule");
            }
        }
        public string INAThirdModule
        {
            get { return _inaThirdModule; }
            set
            {
                _inaThirdModule = value;
                OnPropertyChanged("INAThirdModule");
            }
        }
        public string INAFourthModule
        {
            get { return _inaFourthModule; }
            set
            {
                _inaFourthModule = value;
                OnPropertyChanged("INAFourthModule");
            }
        }
        public INAGroupType SelectedINAGroup {
            get { return _selectedINAGroup; }
            set {
                _selectedINAGroup = value;
                if (value == INAGroupType.Actuators || value == INAGroupType.Motors)
                {
                    INAFirstModule = "Передний левый";
                    INASecondModule = "Передний правый";
                    INAThirdModule = "Задний левый";
                    INAFourthModule = "Задний правый";
                    INAThirdVisibility = Visibility.Visible;
                    INAFourthVisibility = Visibility.Visible;
                }
                else if (value == INAGroupType.Magnets)
                {
                    INAFirstModule = "Передний";
                    INASecondModule = "Задний";
                    INAThirdVisibility = Visibility.Hidden;
                    INAFourthVisibility = Visibility.Hidden;
                } else if (value == INAGroupType.Cameras) {
                    INAFirstModule = "Передняя";
                    INASecondModule = "Задняя";
                    INAThirdVisibility = Visibility.Hidden;
                    INAFourthVisibility = Visibility.Hidden;
                }
                DefineChosenINAModuleNumber();
                OnPropertyChanged("SelectedINAGroup");
            }
        }
        public INAModuleType SelectedINAModule {
            get { return _selectedINAModule; }
            set {
                _selectedINAModule = value;
                OnPropertyChanged("SelectedINAModule");
            }
        }
        public CameraType SelectedCamera
        {
            get { return _selectedCamera; }
            set {
                _selectedCamera = value;
                if (value == CameraType.Front)
                {
                    _selectedCameraID = (int)CommandPreDefinedId.CAN_IDCamFrontCmd;
                }
                else {
                    _selectedCameraID = (int)CommandPreDefinedId.CAN_IDCamBackCmd;
                }
                OnPropertyChanged("SelectedCamera");
            }
        }
        public CameraBlockType SelectedCameraBlock
        {
            get { return _selectedCameraBlock; }
            set
            {
                _selectedCameraBlock = value;
                OnPropertyChanged("SelectedCameraBlock");
            }
        }
        public ActuatorBlockType SelectedActuator {
            get { return _selectedActuator; }
            set {
                _selectedActuator = value;
                switch (value) {
                    case ActuatorBlockType.FrontLeft: _selectedActuatorsID = 12; break;
                    case ActuatorBlockType.BackLeft: _selectedActuatorsID = 13; break;
                    case ActuatorBlockType.FrontRight: _selectedActuatorsID = 14; break;
                    case ActuatorBlockType.BackRight: _selectedActuatorsID = 15; break;
                    case ActuatorBlockType.All: _selectedActuatorsID = 0; break;
                }
                OnPropertyChanged("SelectedActuator");
            }
        }
        public MotorBlockType SelectedMotor {
            get { return _selectedMotor; }
            set {
                _selectedMotor = value;
                switch (value) {
                    case MotorBlockType.FrontRight: _selectedMotorID = 0; break;
                    case MotorBlockType.BackLeft: _selectedMotorID = 1; break;
                    case MotorBlockType.FrontLeft: _selectedMotorID = 2; break;
                    case MotorBlockType.BackRight: _selectedMotorID = 3; break;
                }
                OnPropertyChanged("SelectedMotor");
            }
        }
        public MagnetBlockType SelectedMagnet {
            get { return _selectedMagnet; }
            set {
                _selectedMagnet = value;
                if (value == MagnetBlockType.Back)
                {
                    _selectedMagnetID = 5;
                }
                else {
                    _selectedMagnetID = 4;
                }
                OnPropertyChanged("SelectedMagnet");
            }
        }
        #endregion

        #region ListBoxes Binding (public)
        public List<string> EnteredCommands
        {
            get { return _enteredCommands; }
            set {
                _enteredCommands = value;
                OnPropertyChanged("EnteredCommands");
            }
        }
        public List<string> GeneralParameters
        {
            get { return _generalParameters; }
            set {
                _generalParameters = value;
                OnPropertyChanged("GeneralParameters");
            }
        }
        public Dictionary<string, Dictionary<string, string>> UnitParameters
        {
            get { return _unitParameters; }
            set {
                _unitParameters = value;
                OnPropertyChanged("UnitParameters");
            }
        }
        public List<string> CurrentCommandsList {
            get { return _currentCommandsList; }
            set {
                _currentCommandsList = value;
                OnPropertyChanged("CurrentCommandsList");
            }
        }
        public List<string> UnitParametersForDisplay {
            get { return _unitparametersForDisplay; }
            set {
                _unitparametersForDisplay = value;
                OnPropertyChanged("UnitParametersForDisplay");
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region TextBoxes Binding (public)
        //Main Window
        public string MovementVelocity {
            get { return _movementVelocity; }
            set {
                _movementVelocity = value;
                OnPropertyChanged("MovementVelocity");
            }
        }
        public string StepDistance {
            get { return _stepDistance; }
            set {
                _stepDistance = value;
                OnPropertyChanged("StepDistance");
            }
        }
        //INA
        public string INACurrent {
            get { return _inaCurrent; }
            set {
                _inaCurrent = value;
                OnPropertyChanged("INACurrent");
            }
        }
        public string INADuration {
            get { return _inaDuration; }
            set {
                _inaDuration = value;
                OnPropertyChanged("INADuration");
            }
        }
        //Camera
        public string CameraPosition {
            get { return _cameraPosition; }
            set {
                _cameraPosition = value;
                OnPropertyChanged("CameraPosition");
            }
        }
        public string LightBrigtness {
            get { return _lightBrigtness; }
            set {
                _lightBrigtness = value;
                OnPropertyChanged("LightBrigtness");
            }
        }
        //Actuators
        public string ActuatorStepDistance {
            get { return _actuatorStepDistance; }
            set {
                _actuatorStepDistance = value;
                OnPropertyChanged("ActuatorStepDistance");
            }
        }
        public string ActuatorVelocity {
            get { return _actuatorVelocity; }
            set {
                _actuatorVelocity = value;
                OnPropertyChanged("ActuatorVelocity");
            }
        }
        public string ActuatorCurrent {
            get { return _actuatorCurrent; }
            set {
                _actuatorCurrent = value;
                OnPropertyChanged("ActuatorCurrent");
            }
        }
        public string ReboundValue {
            get { return _reboundValue; }
            set {
                _reboundValue = value;
                OnPropertyChanged("ReboundValue");
            }
        }
        //Motors
        public string LeftMotorPosition {
            get { return _leftMotorPosition; }
            set {
                _leftMotorPosition = value;
                OnPropertyChanged("LeftMotorPosition");
            }
        }
        public string RightMotorPosition {
            get { return _rightMotorPosition; }
            set {
                _rightMotorPosition = value;
                OnPropertyChanged("RightMotorPosition");
            }
        }
        //Movement
        public double XMovingCoordinate {
            get { return _xMovementCoordinate; }
            set {
                _xMovementCoordinate = value;
                OnPropertyChanged("XMovingCoordinate");
            }
        }
        public double YMovingCoordinate {
            get { return _yMovementCoordinate; }
            set {
                _yMovementCoordinate = value;
                OnPropertyChanged("YMovingCoordinate");
            }
        }
        //Magnet
        public string MagnetDegreeOfShutdown {
            get { return _magnetDegreeOfShutdown; }
            set {
                _magnetDegreeOfShutdown = value;
                OnPropertyChanged("MagnetDegreeOfShutdown");
            }
        }
        //Professional Mode
        public string ProId
        {
            get { return _proId; }
            set {
                _proId = value;
                OnPropertyChanged("ProId");
            }
        }
        public string ProDlc {
            get { return _proDlc; }
            set {
                _proDlc = value;
                OnPropertyChanged("ProDlc");
            }
        }
        public string ProData0 {
            get { return _proData0; }
            set {
                _proData0 = value;
                OnPropertyChanged("ProData0");
            }
        }
        public string ProData1
        {
            get { return _proData1; }
            set
            {
                _proData1 = value;
                OnPropertyChanged("ProData1");
            }
        }
        public string ProData2
        {
            get { return _proData2; }
            set
            {
                _proData2 = value;
                OnPropertyChanged("ProData2");
            }
        }
        public string ProData3
        {
            get { return _proData3; }
            set
            {
                _proData3 = value;
                OnPropertyChanged("ProData3");
            }
        }
        public string ProData4
        {
            get { return _proData4; }
            set
            {
                _proData4 = value;
                OnPropertyChanged("ProData4");
            }
        }
        public string ProData5
        {
            get { return _proData5; }
            set
            {
                _proData5 = value;
                OnPropertyChanged("ProData5");
            }
        }
        public string ProData6
        {
            get { return _proData6; }
            set
            {
                _proData6 = value;
                OnPropertyChanged("ProData6");
            }
        }
        public string ProData7
        {
            get { return _proData7; }
            set
            {
                _proData7 = value;
                OnPropertyChanged("ProData7");
            }
        }
        #endregion

        #region ButtonContent Binding (public)
        #endregion

        #region Button Commands (public)
        // Main window commands
        public ICommand ButtonConnectCommand { get; set; }
        public ICommand ButtonClearCommand { get; set; }
        public ICommand ButtonMagnetOnCommand { get; set; }
        public ICommand ButtonMagnetOffCommand { get; set; }
        public ICommand ButtonMakeVerticalStepCommand { get; set; }
        public ICommand ButtonMakeHorizontalStepCommand { get; set; }
        public ICommand ButtonMovementCommand { get; set; }
        public ICommand ButtonStopCommand { get; set; }
        // INA commands
        public ICommand ButtonINAallProtRequestCommand { get; set; }
        public ICommand ButtonINATurnInfoCommand { get; set; }
        public ICommand ButtonINASetProtCommand { get; set; }
        public ICommand ButtonINASetMediumProtCommand { get; set; }
        public ICommand ButtonINASetHighProtCommand { get; set; }
        // General commands
        public ICommand ButtonActuatorsSupplyCommand { get; set; }
        public ICommand ButtonCamerasSupplyCommand { get; set; }
        public ICommand ButtonMotorsSupplyCommand { get; set; }
        public ICommand ButtonPhaseArraySupplyCommand { get; set; }
        // Commands for camera module
        public ICommand ButtonCamExistenceCommand { get; set; }
        public ICommand ButtonCamBrightnessSetCommand { get; set; }
        public ICommand ButtonCamCheckCommand { get; set; }
        public ICommand ButtonCamTurnOnCommand { get; set; }
        public ICommand ButtonCamTurnOffCommand { get; set; }
        public ICommand ButtonCamPositionRequestCommand { get; set; }
        public ICommand ButtonCamCalibrationCommand { get; set; }
        public ICommand ButtonCamSetPositionCommand { get; set; }
        // Commands for actuators module
        public ICommand ButtonActuatorCheckConditionCommand { get; set; }
        public ICommand ButtonActuatorSetMaxMovementCommand { get; set; }
        public ICommand ButtonActuatorRaiseCommand { get; set; }
        public ICommand ButtonActuatorDownCommand { get; set; }
        public ICommand ButtonActuatorMoveCommand { get; set; }
        public ICommand ButtonActuatorStopCommand { get; set; }
        // Commands for motor module
        public ICommand ButtonMotorRequestCommand { get; set; }
        public ICommand ButtonMotorZeroingCommand { get; set; }
        public ICommand ButtonMotorSetEdgeCommand { get; set; }
        // Commands for magnet module
        public ICommand ButtonUniMagnetOnCommand { get; set; }
        public ICommand ButtonUniMagnetOffCommand { get; set; }
        public ICommand ButtonUniMagnetPartialOffCommand { get; set; }
        // Commands for professional mode
        public ICommand ButtonProSendCommand { get; set; }
        #endregion

        #region Constructor
        public ViewModel()
        {
            Application.Current.MainWindow.Closing += new CancelEventHandler(MainWindow_Closing);
            DetermineInitialFieldsConditions();
            InitializeCollections();
            DetermineButtonActions();
        }
        #endregion

        #region View Model Initial State Determing (private)
        private void DetermineInitialFieldsConditions() {
            SelectedCamera = CameraType.Front;
            SelectedActuator = ActuatorBlockType.All;
            SelectedMagnet = MagnetBlockType.Front;
            SelectedMotor = MotorBlockType.FrontLeft;
            SelectedNumericMode = NumericMode.Decimal;
            SelectedINAGroup = INAGroupType.Motors;
            SelectedINAModule = INAModuleType.First;
            //Professional mode fields
            ProId = "336";
            ProDlc = "2";
            ProData0 = "1";
            ProData1 = "1";
            ProData2 = "0";
            ProData3 = "0";
            ProData4 = "0";
            ProData5 = "0";
            ProData6 = "0";
            ProData7 = "0";
        }
        private void InitializeCollections() {
            UnitParametersForDisplay = new List<string>();
            _generalCommandController = new CommandControllerGeneral();
            _unitCommandController = new CommandControllerUnit();
            _decodingController = new DecodingController(new CANPack());
            _unitDecodingController = new DecodingController(new CANPack());
            EnteredCommands = new List<string>();
        }
        private void DetermineButtonActions()
        {
            //Main window
            ButtonConnectCommand = new RelayCommand(o => ButtonConnect("Connect"));           
            ButtonClearCommand = new RelayCommand(o => ButtonClear("Clear"));
            ButtonMakeVerticalStepCommand = new RelayCommand(o => ButtonVerticalStep("MakeVerticalStep"));
            ButtonMakeHorizontalStepCommand = new RelayCommand(o => ButtonHorizontalStep("MakeHorizontalStep"));
            ButtonStopCommand = new RelayCommand(o => ButtonStop("Stop"));
            ButtonMagnetOnCommand = new RelayCommand(o => ButtonMagnetOn("MagnetOn"));
            ButtonMagnetOffCommand = new RelayCommand(o => ButtonMagnetOff("MagnetOff"));
            //INA
            ButtonINAallProtRequestCommand = new RelayCommand(o => ButtonAllProtectionRequest("AllCurrentProtections"));
            ButtonINATurnInfoCommand = new RelayCommand(o => ButtonTurnInformation("TurnInformation"));
            ButtonINASetProtCommand = new RelayCommand(o => ButtonSetProtection("SetProtection"));
            ButtonINASetMediumProtCommand = new RelayCommand(o => ButtonSetMediumProtection("SetMediumProtection"));
            ButtonINASetHighProtCommand = new RelayCommand(o => ButtonSetHighProtection("SetHighProtection"));
            //General
            ButtonActuatorsSupplyCommand = new RelayCommand(o => ButtonActuatorsSupply("ActuatorsSupply"));
            ButtonCamerasSupplyCommand = new RelayCommand(o => ButtonCamerasSupply("CamerasSupply"));
            ButtonMotorsSupplyCommand = new RelayCommand(o => ButtonMotorsSupply("MotorsSupply"));
            ButtonPhaseArraySupplyCommand = new RelayCommand(o => ButtonPhaseArraySupply("PhaseArraySupply"));
            //Camera module
            ButtonCamExistenceCommand = new RelayCommand(o => ButtonCameraExistence("CameraExistence"));
            ButtonCamBrightnessSetCommand = new RelayCommand(o => ButtonCameraBrightnessSet("BrightnessSet"));
            ButtonCamCheckCommand = new RelayCommand(o => ButtonCameraModuleCheck("ModuleCheck"));
            ButtonCamTurnOnCommand = new RelayCommand(o => ButtonCameraTurnOn("CameraTurnOn"));
            ButtonCamTurnOffCommand = new RelayCommand(o => ButtonCameraTurnOff("CameraTurnOff"));
            ButtonCamPositionRequestCommand = new RelayCommand(o => ButtonCameraPositionRequest("PositionRequest"));
            ButtonCamCalibrationCommand = new RelayCommand(o => ButtonCameraCalibration("CameraCalibration"));
            ButtonCamSetPositionCommand = new RelayCommand(o => ButtonCameraSetPosition("CameraSetPosition"));
            //Actuators module
            ButtonActuatorCheckConditionCommand = new RelayCommand(o => ButtonActuatorsCheckCondition("ActuatorsCheck"));
            ButtonActuatorSetMaxMovementCommand = new RelayCommand(o => ButtonActuatorsSetMaxMotion("ActuatorsSetMaxMotion"));
            ButtonActuatorRaiseCommand = new RelayCommand(o => ButtonActuatorsRaise("RaiseActuator"));
            ButtonActuatorDownCommand = new RelayCommand(o => ButtonActuatorsDown("DownActuator"));
            ButtonActuatorStopCommand = new RelayCommand(o => ButtonActuatorsStop("StopActuator"));
            //Motors module
            ButtonMotorRequestCommand = new RelayCommand(o => ButtonMotorRequest("MotorRequestDistance"));
            ButtonMotorZeroingCommand = new RelayCommand(o => ButtonMotorZeroing("ZeroingDistance"));
            ButtonMotorSetEdgeCommand = new RelayCommand(o => ButtonMotorSetEdge("SetEdgePosition"));
            //Magnets module
            ButtonUniMagnetOffCommand = new RelayCommand(o => ButtonMagnetUniOn("MagnetUniOff"));
            ButtonUniMagnetOnCommand = new RelayCommand(o => ButtonMagnetUniOn("MagnetUniOn"));
            ButtonUniMagnetPartialOffCommand = new RelayCommand(o => ButtonMagnetUniPartialOff("MagnetUniPartialOff"));
            //Professional mode
            ButtonProSendCommand = new RelayCommand(o => ButtonSendProCommand("SendProCommand"));
        }

        #endregion

        #region Main window actions (private)
        private void ButtonConnect(object sender)
        {
            Robot = new EthernetDevice(RobotStandardIP, RobotStandardPort);
            GeneralDataObtaining();
            Thread.Sleep(200);
            ObtainingStartParameters();
        }
        private void ButtonClear(object sender)
        {
            UnitParameters = new Dictionary<string, Dictionary<string, string>>();
            UnitParametersForDisplay = new List<string>();
            EnteredCommands = new List<string>();
        }
        private void ButtonVerticalStep(object sender) {
            byte[] distance = BitConverter.GetBytes(Convert.ToInt16(StepDistance));
            _unitCommandController.AddParameterizedCommandToPack((long)CrossParameterizedCommands.MakeVerticalStep, (int)CommandPreDefinedId.CAN_IDCross3_5Cmd, 1, new byte[] { Convert.ToByte(MovementVelocity), distance[0], distance[1] });
            CANPack result = _unitCommandController.SendCommandPack(Robot);
            UpdateListBoxes("Вертикальный шаг.Расстояние: " + Convert.ToInt16(StepDistance) + "  Скорость: " + Convert.ToByte(MovementVelocity), result);
        }
        private void ButtonHorizontalStep(object sender) {
            byte[] distance = BitConverter.GetBytes(Convert.ToInt16(StepDistance)); 
            _unitCommandController.AddParameterizedCommandToPack((long)CrossParameterizedCommands.MakeHorizontalStep, (int)CommandPreDefinedId.CAN_IDCross3_5Cmd, 1,new byte[] {Convert.ToByte(MovementVelocity), distance[0], distance[1] });
            CANPack result = _unitCommandController.SendCommandPack(Robot);
            UpdateListBoxes("Горизонтальный шаг.Расстояние: "+ Convert.ToInt16(StepDistance) + "  Скорость: "+ Convert.ToByte(MovementVelocity), result);
        }
        private void ButtonMagnetOn(object sender) {
            _unitCommandController.AddCommandToPack((long)CrossUnparameterizedCommands.MagnetOn, (int)CommandPreDefinedId.CAN_IDCrossCmd);
            CANPack result = _unitCommandController.SendCommandPack(Robot,5000);
            UpdateListBoxes("Попытка примагнититься", result);
        }
        private void ButtonMagnetOff(object sender) {
            _unitCommandController.AddCommandToPack((long)CrossUnparameterizedCommands.MagnetOff, (int)CommandPreDefinedId.CAN_IDCrossCmd);
            CANPack result = _unitCommandController.SendCommandPack(Robot,5000);
            UpdateListBoxes("Попытка отмагнититься", result);
        }
        private void ButtonMovement(object sender) {
            _unitCommandController.AddParameterizedCommandToPack((long)CrossParameterizedCommands.StartMoving, (int)CommandPreDefinedId.CAN_IDCross3_5Cmd, 1, new byte[] { Convert.ToByte(MovementVelocity)});
            CANPack result = _unitCommandController.SendCommandPack(Robot);
            UpdateListBoxes("Начало движения", result);
        }
        private void ButtonStop(object sender) {
            _unitCommandController.AddCommandToPack((long)CrossUnparameterizedCommands.ActiveStop, (int)CommandPreDefinedId.CAN_IDCross3_5Cmd);
            CANPack result = _unitCommandController.SendCommandPack(Robot);
            UpdateListBoxes("Активный стоп произведен", result);
        }
        #endregion

        #region INA actions (private)
        private void ButtonAllProtectionRequest(object sender) {
            _unitCommandController.AddCommandToPack((long)INAUnParameterizedCommands.INAALLCurrentProtRequest,(int)CommandPreDefinedId.CAN_IDUniCmd);
            CANPack result = _unitCommandController.SendCommandPack(Robot);
            UpdateListBoxes("Запрос состояний токовых защит", result);
        }
        private void ButtonTurnInformation(object sender) {
            _unitCommandController.AddParameterizedCommandToPack((long)INAParameterizedCommands.INATurnInformation,
                (int)CommandPreDefinedId.CAN_IDUniCmd,2,new byte[] {_selectedInaModuleID,1});
            CANPack result = _unitCommandController.SendCommandPack(Robot);
            UpdateListBoxes("Включаем информацию INA для модуля", result);
        }
        private void ButtonSetProtection(object sender) {
            byte [] tmpCurrent = BitConverter.GetBytes(Convert.ToByte(INACurrent));
            _unitCommandController.AddParameterizedCommandToPack((long)INAParameterizedCommands.INASetProtection,
                (int)CommandPreDefinedId.CAN_IDUniCmd,2,new byte[] {_selectedInaModuleID });
            CANPack result = _unitCommandController.SendCommandPack(Robot);
            UpdateListBoxes("Устанавливаем уровень защиты по току", result);
        }
        private void ButtonSetMediumProtection(object sender) {
            byte[] tmpCurrent = BitConverter.GetBytes(Convert.ToByte(INACurrent));
            byte[] tmpDuration = BitConverter.GetBytes(Convert.ToByte(INADuration));
            _unitCommandController.AddParameterizedCommandToPack((long)INAParameterizedCommands.INASetProtection,
                (int)CommandPreDefinedId.CAN_IDUniCmd, 2, new byte[] { _selectedInaModuleID, tmpCurrent[0], tmpCurrent[1], tmpDuration[0], tmpDuration[1]});
            CANPack result = _unitCommandController.SendCommandPack(Robot);
            UpdateListBoxes("Устанавливаем уровень софтовой защиты средний", result);
        }
        private void ButtonSetHighProtection(object sender) {
            byte[] tmpCurrent = BitConverter.GetBytes(Convert.ToByte(INACurrent));
            byte[] tmpDuration = BitConverter.GetBytes(Convert.ToByte(INADuration));
            _unitCommandController.AddParameterizedCommandToPack((long)INAParameterizedCommands.INASetProtection,
                (int)CommandPreDefinedId.CAN_IDUniCmd, 2, new byte[] { _selectedInaModuleID, tmpCurrent[0], tmpCurrent[1], tmpDuration[0], tmpDuration[1] });
            CANPack result = _unitCommandController.SendCommandPack(Robot);
            UpdateListBoxes("Устанавливаем уровень софтовой защиты высокий", result);
        }
        #endregion

        #region General actions (private)
        private void ButtonActuatorsSupply(object sender) {

        }
        private void ButtonCamerasSupply(object sender) {
            
        }
        private void ButtonMotorsSupply(object sender) {

        }
        private void ButtonPhaseArraySupply(object sender) {

        }
        #endregion

        #region Camera module actions (private)
        private void ButtonCameraModuleCheck(object sender) {
            long CommandCode = 0;
            switch (SelectedCameraBlock)
            {
                case CameraBlockType.Laser: CommandCode = (long)CameraCommandsCode.LaserStatusRequest; break;
                case CameraBlockType.LeftLight: CommandCode = (long)CameraCommandsCode.GetLeftLightBright; break;
                case CameraBlockType.RightLight: CommandCode = (long)CameraCommandsCode.GetRightLightBright; break;
                case CameraBlockType.GasIndicator: CommandCode = (long)CameraCommandsCode.GetGasIndicatorDigital; break;
            }
            _unitCommandController.AddCommandToPack(CommandCode, _selectedCameraID);
            CANPack result = _unitCommandController.SendCommandPack(Robot);
            UpdateListBoxes("Проверка выбранного модуля камеры", result);
        }
        private void ButtonCameraTurnOn(object sender) {
            long CommandCode = 0;
            switch (SelectedCameraBlock) {
                case CameraBlockType.Laser: CommandCode = (long)CameraCommandsCode.TurnLaserTrue; break;
                case CameraBlockType.LeftLight: CommandCode = (long)CameraCommandsCode.TurnOnLeftLight; break;
                case CameraBlockType.RightLight: CommandCode = (long)CameraCommandsCode.TurnOnRightLight; break;
            }
            _unitCommandController.AddCommandToPack(CommandCode, _selectedCameraID);
            CANPack result = _unitCommandController.SendCommandPack(Robot);
            UpdateListBoxes("Включение модуля",result);
        }
        private void ButtonCameraTurnOff(object sender) {
            long CommandCode = 0;
            switch (SelectedCameraBlock)
            {
                case CameraBlockType.Laser: CommandCode = (long)CameraCommandsCode.TurnLaserFalse; break;
                case CameraBlockType.LeftLight: CommandCode = (long)CameraCommandsCode.TurnOffLeftLight; break;
                case CameraBlockType.RightLight: CommandCode = (long)CameraCommandsCode.TurnOffRightLight; break;
            }
            _unitCommandController.AddCommandToPack(CommandCode, _selectedCameraID);
            CANPack result = _unitCommandController.SendCommandPack(Robot);
            UpdateListBoxes("Выключение модуля", result);
        }
        private void ButtonCameraPositionRequest(object sender) {
            if (SelectedCamera == CameraType.Front)
            {
                _unitCommandController.AddCommandToPack((long)CameraCommandsCode.CameraFrontPostitionRequest, (int)CommandPreDefinedId.CAN_IDUniCmd);
            }
            else {
                _unitCommandController.AddCommandToPack((long)CameraCommandsCode.CameraBackPositionRequest, (int)CommandPreDefinedId.CAN_IDUniCmd);
            }
            CANPack result = _unitCommandController.SendCommandPack(Robot);
            UpdateListBoxes("Запрос положения камеры", result);
        }
        private void ButtonCameraCalibration(object sender) {
            if (SelectedCamera == CameraType.Front)
            {
                _unitCommandController.AddCommandToPack((long)CameraCommandsCode.CameraFrontCalibrationRequest, (int)CommandPreDefinedId.CAN_IDUniCmd);
            }
            else {
                _unitCommandController.AddCommandToPack((long)CameraCommandsCode.CameraBackCalibrationRequest, (int)CommandPreDefinedId.CAN_IDUniCmd);
            }
            CANPack result = _unitCommandController.SendCommandPack(Robot,22000);
            UpdateListBoxes("Калибровка камеры", result);
        }
        private void ButtonCameraSetPosition(object sender) {
            if (SelectedCamera == CameraType.Front)
            {
                _unitCommandController.AddParameterizedCommandToPack((long)CameraCommandsCode.CameraFrontChangePosition,
                    (int)CommandPreDefinedId.CAN_IDUniCmd, 3, new byte[] { Convert.ToByte(CameraPosition) });
            }
            else {
                _unitCommandController.AddParameterizedCommandToPack((long)CameraCommandsCode.CameraBackChangePosition,
                    (int)CommandPreDefinedId.CAN_IDUniCmd, 3, new byte[] { Convert.ToByte(CameraPosition) });
            }
            CANPack result = _unitCommandController.SendCommandPack(Robot,1000);
            UpdateListBoxes("Изменение положения камеры",result);
        }
        private void ButtonCameraExistence(object sender) {
            _unitCommandController.AddCommandToPack((long)CameraCommandsCode.CheckCameraModule, _selectedCameraID);
            CANPack result = _unitCommandController.SendCommandPack(Robot);
            UpdateListBoxes("Проверка наличия камеры",result);
        }
        private void ButtonCameraBrightnessSet(object sender) {
            if (SelectedCameraBlock == CameraBlockType.LeftLight)
            {
                _unitCommandController.AddParameterizedCommandToPack((long)CameraCommandsCode.SetLeftLightBright,
                    _selectedCameraID, 2, BitConverter.GetBytes(Convert.ToInt16(_lightBrigtness)));
            }
            else if (SelectedCameraBlock == CameraBlockType.RightLight)
            {
                _unitCommandController.AddParameterizedCommandToPack((long)CameraCommandsCode.SetRightLightBright,
                    _selectedCameraID, 2, BitConverter.GetBytes(Convert.ToInt16(_lightBrigtness)));
            }
            CANPack result = _unitCommandController.SendCommandPack(Robot);
            UpdateListBoxes("Установка яркости фары", result);
        }
        #endregion

        #region Actuators actions (private)
        private void ButtonActuatorsCheckCondition(object sender) {
            _unitCommandController.AddCommandToPack((long)ActuatorsUnParameterizedCommands.ActuatorCheckState, (int)CommandPreDefinedId.CAN_IDAwacs3_5Cmd);
            CANPack result = _unitCommandController.SendCommandPack(Robot);
            UpdateListBoxes("Запрос состояния актуаторов", result);
        }
        private void ButtonActuatorsSetMaxMotion(object sender) {
            SelectedActuator = ActuatorBlockType.All;
            IsActuatorCurrentSelected = false;
            IsActuatorDistanceSelected = true;
            IsActuatorReboundSelected = false;
            IsActuatorVelocitySelected = false;
            _unitCommandController.AddParameterizedCommandToPack((long)ActuatorsParameterizedCommands.ActuatorsDownRebound,
                (int)CommandPreDefinedId.CAN_IDAwacs3_5Cmd, 2, new byte[] {Convert.ToByte(ActuatorStepDistance) } );
            CANPack result = _unitCommandController.SendCommandPack(Robot);
            UpdateListBoxes("Установка длины макс.хода актуаторов", result);
        }
        private void ButtonActuatorsRaise(object sender) {
            SelectedActuator = ActuatorBlockType.All;
            string AdditionalInformation = "";
            if (!IsActuatorCurrentSelected)
            {
                _unitCommandController.AddCommandToPack((long)ActuatorsUnParameterizedCommands.ActuatorsRaise, (int)CommandPreDefinedId.CAN_IDAwacs3_5Cmd);
            }
            else if (IsActuatorCurrentSelected)
            {
                _unitCommandController.AddParameterizedCommandToPack((long)ActuatorsParameterizedCommands.ActuatorsRaiseCurrent,
                    (int)CommandPreDefinedId.CAN_IDAwacs3_5Cmd, 2, BitConverter.GetBytes(Convert.ToInt16(ActuatorCurrent)));
                AdditionalInformation = ".Выбран ток:" + ActuatorCurrent;
            }
            IsActuatorDistanceSelected = false;
            IsActuatorReboundSelected = false;
            IsActuatorVelocitySelected = false;
            CANPack result = _unitCommandController.SendCommandPack(Robot);
            UpdateListBoxes("Поднятие актуаторов"+AdditionalInformation, result);
        }
        private void ButtonActuatorsDown(object sender) {
            SelectedActuator = ActuatorBlockType.All;
            string AdditionalInformation = "";
            if (IsActuatorReboundSelected && !IsActuatorCurrentSelected)
            {
                _unitCommandController.AddParameterizedCommandToPack((long)ActuatorsParameterizedCommands.ActuatorsDownRebound,
                    (int)CommandPreDefinedId.CAN_IDAwacs3_5Cmd, 2, BitConverter.GetBytes(Convert.ToByte(ReboundValue)));
                AdditionalInformation = ".Выбран отскок:" + ReboundValue;
            }
            else if (IsActuatorReboundSelected && IsActuatorCurrentSelected)
            {
                byte [] tmpCurrent = BitConverter.GetBytes(Convert.ToInt16(ActuatorCurrent));
                _unitCommandController.AddParameterizedCommandToPack((long)ActuatorsParameterizedCommands.ActuatorsDownReboundCurrent,
                    (int)CommandPreDefinedId.CAN_IDAwacs3_5Cmd, 2, new byte[] {Convert.ToByte(ReboundValue), tmpCurrent[0], tmpCurrent[1] });
                AdditionalInformation = ".Выбран отскок:" + ReboundValue +" И ток:" + ActuatorCurrent;
            }
            else if (!IsActuatorReboundSelected && !IsActuatorCurrentSelected) {
                MessageBox.Show("Не было выбрано ни одного параметра, необходимого для этой операции. Выберите отскок и/или ток и попробуйте снова.");
                return;
            }
            IsActuatorReboundSelected = false;
            IsActuatorVelocitySelected = false;
            CANPack result = _unitCommandController.SendCommandPack(Robot);
            UpdateListBoxes("Опускание актуаторов"+AdditionalInformation, result);
        }
        private void ButtonActuatorsMovement(object sender) {
            byte[] tmpDistance = BitConverter.GetBytes(Convert.ToInt16(ActuatorStepDistance));
            byte[] tmpCurrent = BitConverter.GetBytes(Convert.ToInt16(ActuatorCurrent));
            long command = 0;
            byte[] commandParameters = new byte[] { };
            if (IsActuatorCurrentSelected && IsActuatorDistanceSelected && IsActuatorVelocitySelected)
            {
                if (SelectedActuator == ActuatorBlockType.All)
                {
                    command = (long)ActuatorsParameterizedCommands.ActuatorsMoveDistVelCurrent;
                    commandParameters = new byte[] { tmpDistance[0], tmpDistance[1], Convert.ToByte(ActuatorVelocity), tmpCurrent[0], tmpCurrent[1] };
                }
                else {
                    command = (long)ActuatorsParameterizedCommands.ActuatorOneMoveDistVelCurrent;
                    commandParameters = new byte[] { _selectedActuatorsID, tmpDistance[0], tmpDistance[1], Convert.ToByte(ActuatorVelocity), tmpCurrent[0], tmpCurrent[1] };
                }
                _unitCommandController.AddParameterizedCommandToPack(command, (int)CommandPreDefinedId.CAN_IDAwacs3_5Cmd, 2, commandParameters);
            }
            else if (IsActuatorCurrentSelected && IsActuatorDistanceSelected && !IsActuatorVelocitySelected)
            {
                if (SelectedActuator == ActuatorBlockType.All) {
                    return;
                }
                commandParameters = new byte[] { _selectedActuatorsID, Convert.ToByte(ActuatorStepDistance), tmpCurrent[0], tmpCurrent[1] };
                _unitCommandController.AddParameterizedCommandToPack((long)ActuatorsParameterizedCommands.ActuatorMoveDistanceCurrent,
                    (int)CommandPreDefinedId.CAN_IDAwacs3_5Cmd, 2, commandParameters);
            }
            else if ((!IsActuatorCurrentSelected || !IsActuatorDistanceSelected) && !IsActuatorVelocitySelected)
            {
                MessageBox.Show("Не было выбрано ни одного параметра, необходимого для этой операции. Обязательно выбрать дистанцию и ток, также можно выбрать скорость. Попробуйте снова.");
                return;
            }
            CANPack result = _unitCommandController.SendCommandPack(Robot);
            UpdateListBoxes("Движение актуаторов", result);
        }
        private void ButtonActuatorsStop(object sender) {
            byte ActuatorNumber = 1;
            switch (SelectedActuator) {
                case ActuatorBlockType.All: return;
                case ActuatorBlockType.BackLeft: ActuatorNumber = 1; break;
                case ActuatorBlockType.BackRight: ActuatorNumber = 2; break;
                case ActuatorBlockType.FrontLeft: ActuatorNumber = 3; break;
                case ActuatorBlockType.FrontRight: ActuatorNumber = 4; break;
            }
            _unitCommandController.AddParameterizedCommandToPack((long)ActuatorsParameterizedCommands.ActuatorOneStop, (int)CommandPreDefinedId.CAN_IDAwacs3_5Cmd,2,new byte[] {ActuatorNumber} );
            CANPack result = _unitCommandController.SendCommandPack(Robot);
            UpdateListBoxes("Остановка выбранного актуатора", result);
        }
        #endregion

        #region Motor actions (private)
        private void ButtonMotorRequest(object sender) {
            _unitCommandController.AddParameterizedCommandToPack((long)MotorParameterizedCommands.MotorTraveledDistanceRequest,
                (int)CommandPreDefinedId.CAN_IDUniCmd, 2, new byte[] {_selectedMotorID});
            CANPack result = _unitCommandController.SendCommandPack(Robot);
            UpdateListBoxes("Запрос пути мотора", result);
        }
        private void ButtonMotorZeroing(object sender) {
            _unitCommandController.AddParameterizedCommandToPack((long)MotorParameterizedCommands.MotorZeroingDistace,
                (int)CommandPreDefinedId.CAN_IDUniCmd, 2, new byte[] { _selectedMotorID });
            CANPack result = _unitCommandController.SendCommandPack(Robot);
            UpdateListBoxes("Обнуление пути мотора", result);
        }
        private void ButtonMotorSetEdge(object sender) {
            byte [] tmpLeftMaxPosition = BitConverter.GetBytes(Convert.ToInt16(LeftMotorPosition));
            byte [] tmpRightMaxPosition = BitConverter.GetBytes(Convert.ToInt16(RightMotorPosition));
            if (SelectedMotor == MotorBlockType.FrontLeft || SelectedMotor == MotorBlockType.FrontRight)
            {
                _unitCommandController.AddParameterizedCommandToPack((long)MotorParameterizedCommands.MotorFrontSetMaxPositions,
                    (int)CommandPreDefinedId.CAN_IDUniCmd, 2, new byte[] {tmpLeftMaxPosition[0], tmpLeftMaxPosition[1], tmpRightMaxPosition[0], tmpRightMaxPosition[1]});
            }
            else if (SelectedMotor == MotorBlockType.BackLeft || SelectedMotor == MotorBlockType.BackRight) {
                _unitCommandController.AddParameterizedCommandToPack((long)MotorParameterizedCommands.MotorBackSetMaxPositions,
                    (int)CommandPreDefinedId.CAN_IDUniCmd, 2, new byte[] { tmpLeftMaxPosition[0], tmpLeftMaxPosition[1], tmpRightMaxPosition[0], tmpRightMaxPosition[1]});
            }
            CANPack result = _unitCommandController.SendCommandPack(Robot);
            UpdateListBoxes("Установка крайних положений мотора", result);
        }
        #endregion

        #region Magnet actions (private)
        private void ButtonMagnetUniOn(object sender) {
            _unitCommandController.AddCommandToPack((long)MagnetUnParameterizedCommands.MagnetTurnOn, (int)CommandPreDefinedId.CAN_IDUniCmd);
            CANPack result = _unitCommandController.SendCommandPack(Robot);
            UpdateListBoxes("Включение магнита(Uni)", result);
        }
        private void ButtonMagnetUniOff(object sender) {
            _unitCommandController.AddCommandToPack((long)MagnetUnParameterizedCommands.MagnetTurnOff, (int)CommandPreDefinedId.CAN_IDUniCmd);
            CANPack result = _unitCommandController.SendCommandPack(Robot);
            UpdateListBoxes("Выключение магнита(Uni)", result);
        }
        private void ButtonMagnetUniPartialOff(object sender) {
            _unitCommandController.AddParameterizedCommandToPack((long)MagnetParameterizedCommands.MagnetPartialTurnOff,
                (int)CommandPreDefinedId.CAN_IDUniCmd,2, new byte[] {_selectedMagnetID, Convert.ToByte(MagnetDegreeOfShutdown)});
            CANPack result = _unitCommandController.SendCommandPack(Robot);
            UpdateListBoxes("Частичное выключение магнита(Uni)", result);
        }
        #endregion

        #region Scaner Movement actions (public)
        public void ButtonPerformMovement() {
            _unitCommandController.AddParameterizedCommandToPack((long)CrossParameterizedCommands.JoystickMovement, (int)CommandPreDefinedId.CAN_IDCrossCmd, 1 , new byte[] { (byte)(XMovingCoordinate/2.95), (byte)(YMovingCoordinate/2.95)});
            CANPack result = _unitCommandController.SendCommandPack(Robot);
            UpdateListBoxes("Движение с помощью джойстика: X: "+ (byte)(XMovingCoordinate/2.95) + "Y: " + (byte)(YMovingCoordinate/2.95), result);
            MessageBox.Show("X:"+ XMovingCoordinate + "Y:" + YMovingCoordinate);
        }
        #endregion

        #region Professional mode actions (private)
        private void ButtonSendProCommand(object sender) {
            try
            {
                CanFrame proFrame = new CanFrame();
                if (SelectedNumericMode == NumericMode.Decimal)
                {
                    proFrame.id = Convert.ToInt32(ProId);
                    proFrame.dlc = Convert.ToByte(ProDlc);
                    proFrame.reserved = new byte[3];
                    proFrame.data = new byte[] {
                    Convert.ToByte(ProData0), Convert.ToByte(ProData1),
                    Convert.ToByte(ProData2), Convert.ToByte(ProData3),
                    Convert.ToByte(ProData4), Convert.ToByte(ProData5),
                    Convert.ToByte(ProData6), Convert.ToByte(ProData7)
                };
                }
                else if (SelectedNumericMode == NumericMode.Hex)
                {
                    proFrame.id = int.Parse(ProId, System.Globalization.NumberStyles.HexNumber);
                    proFrame.dlc = byte.Parse(ProDlc, System.Globalization.NumberStyles.HexNumber);
                    proFrame.reserved = new byte[3];
                    proFrame.data = new byte[] {
                    byte.Parse(ProData0, System.Globalization.NumberStyles.HexNumber), byte.Parse(ProData1, System.Globalization.NumberStyles.HexNumber),
                    byte.Parse(ProData2, System.Globalization.NumberStyles.HexNumber), byte.Parse(ProData3, System.Globalization.NumberStyles.HexNumber),
                    byte.Parse(ProData4, System.Globalization.NumberStyles.HexNumber), byte.Parse(ProData5, System.Globalization.NumberStyles.HexNumber),
                    byte.Parse(ProData6, System.Globalization.NumberStyles.HexNumber), byte.Parse(ProData7, System.Globalization.NumberStyles.HexNumber)
                };
                }
                _unitCommandController.AddPreparedFrameToPack(proFrame);
                CANPack result = _unitCommandController.SendCommandPack(Robot);
                UpdateListBoxes("ID: " + ProId + " Dlc: " + ProDlc + " Data: " + ProData0 + " | " + ProData1 + " | " + ProData2 + " | " + ProData3 + " | " + ProData4 + " | " + ProData5 + " | " + ProData6 + " | " + ProData7, result);
            } catch (FormatException exc) {
                MessageBox.Show("Try to enter numbers in correct format.(In hex mode without x,0x)");
            }
        }
        #endregion

        #region Window methods(Actions, private)
        private void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            if (GeneralThread != null)
            {
                GeneralThread.Abort();
            }
        }
        #endregion

        #region Main Working Methods (private)
        private void ObtainingStartParameters() {
            _unitCommandController.AddCommandToPack((long)CameraCommandsCode.CheckCameraModule, (int)CommandPreDefinedId.CAN_IDCamFrontCmd);
            _unitCommandController.AddCommandToPack((long)CameraCommandsCode.LaserStatusRequest, (int)CommandPreDefinedId.CAN_IDCamFrontCmd);
            _unitCommandController.AddCommandToPack((long)CameraCommandsCode.GetLeftLightBright, (int)CommandPreDefinedId.CAN_IDCamFrontCmd);
            _unitCommandController.AddCommandToPack((long)CameraCommandsCode.GetRightLightBright, (int)CommandPreDefinedId.CAN_IDCamFrontCmd);
            _unitCommandController.AddCommandToPack((long)CameraCommandsCode.GetGasIndicatorDigital, (int)CommandPreDefinedId.CAN_IDCamFrontCmd);
            _unitCommandController.AddCommandToPack((long)CameraCommandsCode.CheckCameraModule, (int)CommandPreDefinedId.CAN_IDCamBackCmd);
            _unitCommandController.AddCommandToPack((long)CameraCommandsCode.LaserStatusRequest, (int)CommandPreDefinedId.CAN_IDCamBackCmd);
            _unitCommandController.AddCommandToPack((long)CameraCommandsCode.GetLeftLightBright, (int)CommandPreDefinedId.CAN_IDCamBackCmd);
            _unitCommandController.AddCommandToPack((long)CameraCommandsCode.GetRightLightBright, (int)CommandPreDefinedId.CAN_IDCamBackCmd);
            _unitCommandController.AddCommandToPack((long)CameraCommandsCode.GetGasIndicatorDigital, (int)CommandPreDefinedId.CAN_IDCamBackCmd);
            CANPack result = _unitCommandController.SendCommandPack(Robot);
            UpdateListBoxes("Проверка начальных параметров",result);
            //StateUnitParameters = DataConverter.FillStateFromStart(UnitParameters);
        }
        private void GeneralDataObtaining()
        {
            GeneralThread = new Thread(() => {
                while (true)
                {
                    try
                    {
                        if (Robot.ConnectDevice())
                        {
                            CANPack result = _generalCommandController.PerformGeneralCheck(Robot);
                            _decodingController = new DecodingController(result);
                            Dictionary<string, string> generalParam = _decodingController.GetGeneralParameters();
                            if (generalParam.Count > 4)
                            {
                                this.GeneralParameters = DataConverter.PrepareGeneralParameters(_decodingController.GetGeneralParameters());
                            }
                            Robot.DisconnectDevice();
                        }
                    } catch (SocketException e) {
                        MessageBox.Show("Cannot establish connection with Device");
                        return;
                    }
                }
            });
            GeneralThread.Start();
        }
        private void UpdateListBoxes(string CommandName, CANPack result) {
            try
            {
                _unitDecodingController.SetInput_Pack(result);
                List<string> currentCommands = new List<string>();
                for (int i = 0; i < EnteredCommands.Count; i++)
                {
                    currentCommands.Add(EnteredCommands[i]);
                }
                currentCommands.Add(CommandName);
                EnteredCommands = currentCommands;
                UnitParameters = _unitDecodingController.UnitParameters;
                List<string> currentUnitList = new List<string>();
                for (int j = 0; j < UnitParametersForDisplay.Count; j++) {
                    currentUnitList.Add(UnitParametersForDisplay[j]);
                }
                List<string> obtainedNewParameters = DataConverter.PrepareUnitParameters(UnitParameters);
                for (int k = 0; k < obtainedNewParameters.Count; k++) {
                    currentUnitList.Add(obtainedNewParameters[k]);
                }
                if (obtainedNewParameters.Count == 0) currentUnitList.Add("Нет ответа от модуля");
                UnitParametersForDisplay = currentUnitList;
                _unitDecodingController.ClearDecodedData();
                _unitCommandController.ClearCommandPack();
            }
            catch (SocketException e) {
                MessageBox.Show("The Device is not connected. Try to establish connection first.");
            }
        }
        public void OnPropertyChanged(string propertyname)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyname));
        }
        public void DefineChosenINAModuleNumber() {
            byte moduleID = 0;
            switch (_selectedINAGroup)
            {
                case INAGroupType.Actuators:
                    switch (_selectedINAModule)
                    {
                        case INAModuleType.First: moduleID = 14; break;
                        case INAModuleType.Second: moduleID = 12; break;
                        case INAModuleType.Third: moduleID = 13; break;
                        case INAModuleType.Fourth: moduleID = 15; break;
                    }
                    break;
                case INAGroupType.Cameras:
                    if (_selectedINAModule == INAModuleType.First)
                    {
                        moduleID = 8;
                    }
                    else if (_selectedINAModule == INAModuleType.Second)
                    {
                        moduleID = 9;
                    }
                    break;
                case INAGroupType.Magnets:
                    if (_selectedINAModule == INAModuleType.First)
                    {
                        moduleID = 4;
                    }
                    else if (_selectedINAModule == INAModuleType.Second)
                    {
                        moduleID = 5;
                    }
                    break;
                case INAGroupType.Motors:
                    switch (_selectedINAModule)
                    {
                        case INAModuleType.First: moduleID = 0; break;
                        case INAModuleType.Second: moduleID = 2; break;
                        case INAModuleType.Third: moduleID = 1; break;
                        case INAModuleType.Fourth: moduleID = 4; break;
                    }
                    break;
            }
            _selectedInaModuleID = moduleID;
        }
    }
    #endregion
}
