﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandModuleDebugger
{
    public enum INAGroupType {
        Motors,
        Magnets,
        Actuators,
        Cameras
    }
    public enum INAModuleType {
        First,
        Second,
        Third,
        Fourth
    }
    public enum CameraType
    {
        Front,
        Back
    }
    public enum CameraBlockType
    {
        Laser,
        LeftLight,
        RightLight,
        GasIndicator
    }
    public enum ActuatorBlockType
    {
        All,
        FrontLeft,
        FrontRight,
        BackLeft,
        BackRight
    }
    public enum MotorBlockType
    {
        FrontLeft,
        FrontRight,
        BackLeft,
        BackRight
    }
    public enum MagnetBlockType
    {
        Front,
        Back
    }
    public enum NumericMode {
        [Description("Decimal")]
        Decimal,
        [Description("Hex")]
        Hex
    }
    public enum ConnectionMode {
        [Description("Ethernet")]
        Ethernet,
        [Description("WiFi")]
        WiFi,
        [Description("CAN")]
        CAN
    }
}
