﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace CommandModuleDebugger
{
    /// <summary>
    /// The class that specifies the standard interation with ethernet devices that we suppose to use with program.
    /// </summary>
    public class EthernetDevice
    {
        #region constants
        private const int ReceivedLargeTimeoutMS = 10000; // Amount of time that we consider sufficient for all large data receiving.
        private const int VeryLargeTimeoutInReceivingLarge = 10000000; // Don't no exactly, why do we need such timeout of about 10000 seconds. In this case we literaly have infinite timeout.
        #endregion

        #region protected fields
        protected string ipAddress;
        protected int ipPort;
        protected Socket socket;
        protected bool Connected;
        #endregion

        #region Constructors
        public EthernetDevice(string ipAddress, int port) {
            this.ipAddress = ipAddress;
            this.ipPort = port;
        }
        #endregion

        #region public methods
        public void SetAddress(string ipAddress, int port) {
            this.ipAddress = ipAddress;
            this.ipPort = port;
        }
        /// <summary>
        /// Try to connect to our Ethernet Device
        /// </summary>
        /// <returns> result of tryout</returns>
        public bool ConnectDevice() {
            bool result = ConnectSocket(this.ipAddress, this.ipPort, 100000);
            return true;
        }
        /// <summary>
        /// Disconnection from Ethernet device and closure of active socket
        /// </summary>
        public void DisconnectDevice() {
            if (Connected) {
                CloseSocket();
            }
        }
        /// <summary>
        /// Sending data to device. Will not work without connected socket.
        /// </summary>
        /// <param name="sendingData">The array of bytes to send. Usually it is the settings(28bytes), coverted from structure Control Data</param>
        /// <param name="dataSize">Size of data to send</param>
        /// <returns>Result of sending tryout. False if it was not possible to send data or all amount of this data.</returns>
        public bool SendData(byte [] sendingData, int dataSize) {
            bool result = false;
            if (Connected) {
                int sendBytesCount = socket.Send(sendingData, dataSize, SocketFlags.None);
                if (dataSize == sendBytesCount) {
                    result = true;
                }
            }
            return result;
        }
        /// <summary>
        /// Receiving data from device. I found it strange and impossible to return bool from this method. As for me It's quite logical to return the number of received bytes.
        /// This method is running via blocking mode without timeout. So it may cause a problem: program will not work if there is no data to receive.
        /// Will not work without connected socket.
        /// </summary>
        /// <param name="storageArray">Array to storage received bytes</param>
        /// <param name="dataSize">The size of data(number of bytes) that we want to receive</param>
        /// <returns></returns>
        public int ReceiveData(byte [] storageArray, int dataSize) {
            int result = 0;
            if (Connected) {
                result = socket.Receive(storageArray, dataSize,SocketFlags.None );
            }
            return result;
        }
        /// <summary>
        /// Try to receive all available data.
        /// This method is running via blocking mode without timeout. So it may cause a problem: program will not work if there is no data to receive.
        /// Will not work without connected socket.
        /// </summary>
        /// <param name="storageArray">Array to storage received bytes</param>
        /// <returns></returns>
        public int ReceiveData(byte[] storageArray)
        {
            int result = 0;
            if (Connected)
            {
                result = socket.Receive(storageArray);
            }
            return result;
        }
        /// <summary>
        /// Receiving data from device. This method is running via blocking mode with timeout. So the method will throw an exception if the operation will not end in the suggested time.
        /// Will not work without connected socket.
        /// </summary>
        /// <param name="storageArray">Array to storage received bytes</param>
        /// <param name="dataSize">The size of data(number of bytes) that we want to receive</param>
        /// <param name="timeoutUS"></param>
        /// <returns>Amount of time that we consider sufficient for specified size of data that we want to receive</returns>
        public int ReceiveData(byte[] storageArray, int dataSize, int timeoutUS) {
            int result = 0;
            socket.ReceiveTimeout = timeoutUS;
            if (Connected) {
                int startTime = Environment.TickCount;
                result = socket.Receive(storageArray, dataSize, SocketFlags.None);
                if (result == 0)
                {
                    throw new Exception("Error in receiveData.");
                }
            }
            return result;
        }
        /// <summary>
        /// Receiving large amount of data. This method is running via blocking mode with timeout which is given from constant and not specified in the input parameters.
        /// Will not work without connected socket.
        /// </summary>
        /// <param name="storageArray"></param>
        /// <param name="dataSize"></param>
        /// <returns></returns>
        public int ReceiveLargeData(byte [] storageArray, int dataSize) {
            int ReceivedSize = 0;
            if (Connected) {
                int startTime = Environment.TickCount;
                do {
                    byte[] tmp = new byte[dataSize];
                    int PartSize = ReceiveData(tmp, dataSize, VeryLargeTimeoutInReceivingLarge);
                    if (PartSize > 0)
                    {
                        for (int i = 0; i < PartSize; i++) {
                            storageArray[ReceivedSize + i] = tmp[i];
                        }
                        ReceivedSize += PartSize;
                    }
                    else {
                        if (Environment.TickCount - startTime > ReceivedLargeTimeoutMS)
                            break;
                        else
                            continue;
                    }
                } while (ReceivedSize < dataSize);
            }
            return ReceivedSize;
        }
        #endregion

        #region protected methods
        /// <summary>
        /// Closure of socket for connection with the device.
        /// </summary>
        protected void CloseSocket() {
            socket.Shutdown(SocketShutdown.Both);
            socket.Close();
            this.Connected = false;
        }
        /// <summary>
        /// Connection to socket and checking the readiness of it to sending data to the device.
        /// </summary>
        /// <param name="ipAdress">The IP adress of the device to connection establishing</param>
        /// <param name="port">The port of the device to connection establishing</param>
        /// <param name="timeout">The time that we suggest the sufficient to connection establishing</param>
        /// <returns></returns>
        protected bool ConnectSocket(string ipAdress, int port, int timeout) {
            int start = Environment.TickCount;
            this.socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            IPAddress iPAddress = IPAddress.Parse(ipAddress);
            //socket.Blocking = false;
            socket.Connect(iPAddress, port);
            if (!socket.Connected)
            {
                socket.Shutdown(SocketShutdown.Both);
                socket.Close();
                return false;
                throw new Exception("ERROR IN connectSocket. Error in function connect");
            }
            IList checkRead = new ArrayList();
            IList checkWrite = new ArrayList();
            IList checkErr = new ArrayList();
            checkRead.Add(socket);
            checkWrite.Add(socket);
            checkErr.Add(socket);
            Socket.Select(checkRead, checkWrite, checkErr, timeout);
            if (!checkWrite.Contains(socket)) {
                socket.Shutdown(SocketShutdown.Both);
                socket.Close();
            }
            Connected = true;
            return true;
        }
        #endregion
    }
}
