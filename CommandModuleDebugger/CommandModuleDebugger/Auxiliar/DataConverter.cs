﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandModuleDebugger.Auxiliar
{
    public class DataConverter
    {
        public static Dictionary<string, Dictionary<string, string>> BlankParametersDictionary = new Dictionary<string, Dictionary<string, string>> {
            {"Передняя камера.",new Dictionary<string,string>(){
                {"Модуль:","" },
                {"Лазер:" ,""},
                {"Левая фара:" ,""},
                {"Правая фара:" ,""},
            }},
            {"Задняя камера.",new Dictionary<string,string>(){
                {"Модуль:","" },
                {"Лазер:" ,""},
                {"Левая фара:" ,""},
                {"Правая фара:" ,""},
            }},
        };
        public static List<string> PrepareGeneralParameters(Dictionary<string, string> input)
        {
            List<string> result = new List<string>(new string[13]);
            foreach (KeyValuePair<string, string> pair in input)
            {
                if (pair.Key == "Магнитное поле:")
                {
                    result[0] = pair.Key + "   " + pair.Value;
                }
                else if (pair.Key == "Батарея:")
                {
                    result[1] = pair.Key + "   " + pair.Value;
                }
                else if (pair.Key == "Ток:")
                {
                    result[2] = pair.Key + "   " + pair.Value;
                }
                else if (pair.Key == "Число светодиодов:")
                {
                    result[3] = pair.Key + "   " + pair.Value;
                }
                else if (pair.Key == "gx:")
                {
                    result[4] = pair.Key + "   " + pair.Value;
                }
                else if (pair.Key == "gy:")
                {
                    result[5] = pair.Key + "   " + pair.Value;
                }
                else if (pair.Key == "gz:")
                {
                    result[6] = pair.Key + "   " + pair.Value;
                }
                else if (pair.Key == "Состояние защиты INA:") {
                    result[7] = pair.Key + "   " + pair.Value;
                }
                else if (pair.Key == "Угол удержания инклинометра:")
                {
                    result[8] = pair.Key + "   " + pair.Value;
                }
                else if (pair.Key == "Шаг:")
                {
                    result[9] = pair.Key + "   " + pair.Value;
                }
                else if (pair.Key == "Останов по магнитному полю:")
                {
                    result[10] = pair.Key + "   " + pair.Value;
                }
                else if (pair.Key == "Version1:")
                {
                    result[11] = pair.Key + "   " + pair.Value;
                }
                else if (pair.Key == "Version2:")
                {
                    result[12] = pair.Key + "   " + pair.Value;
                }
            }
            return result;
        }
        public static List<string> PrepareUnitParameters(Dictionary<string, Dictionary<string,string>> obtainedParameters) {
            List<string> result = new List<string>(obtainedParameters.Count);
            foreach (KeyValuePair<string, Dictionary<string,string>> pair in obtainedParameters) {
                Dictionary<string, string> tmp = pair.Value;
                string tmpAsString = "";
                foreach (KeyValuePair<string, string> innerPair in tmp) {
                    tmpAsString = innerPair.Key.ToString() +"  "+ innerPair.Value.ToString();
                    result.Add(pair.Key.ToString() + " " + tmpAsString);
                }
            }
            return result;
        }
        public static Dictionary<string, Dictionary<string, string>> FillStateFromStart(Dictionary<string,Dictionary<string,string>> obtainedUnitParams) {
            Dictionary<string, Dictionary<string, string>> result = BlankParametersDictionary;
            foreach (KeyValuePair<string, Dictionary<string,string>> pair in obtainedUnitParams) {
                Dictionary<string, string> innerObtainedDictionary = obtainedUnitParams[pair.Key];
                Dictionary<string, string> resultInnerDictionary = result[pair.Key];
                foreach (KeyValuePair<string, string> innerPair in innerObtainedDictionary) {
                    resultInnerDictionary[innerPair.Key] = innerObtainedDictionary[innerPair.Key];
                }
                result[pair.Key] = resultInnerDictionary;
            }
            return result;
        }
    }
}
