﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandModuleDebugger
{
    /// <summary>
    /// ID of devices to send commands
    /// </summary>
    public enum CommandPreDefinedId {
        CAN_IDCrossCmd = 0x7c2,
        CAN_IDCross3_5Cmd = 0x7c8,
        CAN_IDAwacs3_5Cmd = 0x7c3,
        CAN_IDUniCmd = 0x7c0,
        CAN_IDCamFrontCmd = 0x150,
        CAN_IDCamBackCmd = 0x152,
        CAN_IDTestCmd = 0x705,
    }
    public enum CrossUnparameterizedCommands {
        MagnetOn = 0x0400,
        MagnetOff = 0x0500,
        ActiveStop = 0x0600,
        StepEndingRequest = 0x0800,
        PhaseArrayPowerSupplyTurnOn = 0x0900,
        PhaseArrayPowerSupplyTurnOff = 0x0901,
        PasiveStop = 0x1000,
        ActuatorPowerSupplyTurnOn = 0x0c01,
        ActuatorPowerSupplyTurnOff = 0x0c01,
        CamerasPowerSupplyTurnOff = 0x0d0,
        CamerasPowerSupplyTurnOn = 0x0d00,
        MotorPowerSupplyTurn0n = 0x0e01,
        MotorPowerSupplyTurn0ff = 0x0e00,
        RememberCurrentSteeringAngle = 0x0f00,
        ModemReboot = 0x1100,
    }
    public enum CrossParameterizedCommands {
        JoystickMovement = 0x00, // 1 (X), 2(Y)
        MakeHorizontalStep = 0x01,// 1(V), 2,3(X)
        MakeVerticalStep = 0x02,// 1(V), 2,3(X)
        StartMoving = 0x03, // 1(V)
        TankTurn = 0x110000,//1(V), 2(0/1)
        SetCurrentSteeringAngle = 0x160000, //1,2(угол)
        ActuatorsToPosition = 0x180000,//1,2(Положение)
    }
    public enum ActuatorsUnParameterizedCommands {
        ActuatorCheckState = 0x032e,
        ActuatorsStop = 0x03,
        ActuatorsRaise = 0x0329,
    }
    public enum ActuatorsParameterizedCommands {
        ActuatorsDownRebound = 0x0328,//2(X- rebound)
        ActuatorsDownReboundCurrent = 0x032a,//2(X- rebound),3-4(Current)
        ActuatorsRaiseCurrent = 0x032b,//
        ActuatorsRaiseDistanceCurrent = 0x032c,
        ActuatorMoveDistanceCurrent = 0x032d,
        ActuatorsSetMaxDistance = 0x032e,
        ActuatorPosRequest = 0x032e,
        ActuatorsMoveDistVelCurrent = 0x30, //wtf?????
        ActuatorOneMoveDistVelCurrent = 0x30,
        ActuatorOneStop = 0x03,
    }
    public enum MotorUnParameterizedCommands {

    }
    public enum MotorParameterizedCommands {
        MotorZeroingDistace = 0xff36,
        MotorTraveledDistanceRequest = 0xff37,
        MotorFrontSetMaxPositions = 0x148,
        MotorBackSetMaxPositions = 0x149,
    }
    public enum MagnetUnParameterizedCommands {
        MagnetTurnOn = 0x220,
        MagnetTurnOff = 0x221,
    }
    public enum MagnetParameterizedCommands {
        MagnetPartialTurnOff = 0x222,
    }
    public enum INAUnParameterizedCommands
    {
        INAALLCurrentProtRequest = 0xff39,
    }
    public enum INAParameterizedCommands {
        INATurnOffProtection = 0xff32, //2(ID)
        INASetProtection = 0xff33, //2(ID), 3-4(Current)
        INASetMediumProtection = 0xff34, //2(ID), 3-4(Current), 5-6(Duration)
        INASetHighProtection = 0xff35, //2(ID), 3-4(Current), 5-6(Duration)
        INATurnInformation = 0xff38, //2(ID), 3(state 0/1)
    }
    public enum BaseComandsCode {
        MagnetOn = 0x04,
        MagnetOff = 0x05,
        GridDown2 = 0x034000,
        GridUp = 0x0341,
        GridDownCurrentDef = 0x0342,
    }
    public enum CameraCommandsCode {
        CheckCameraModule = 0x0101, 
        TurnOnRightLight = 0x0305,
        TurnOnLeftLight = 0x0304,
        TurnOffLeftLight = 0x0306,
        TurnOffRightLight = 0x0307,
        TurnLaserTrue = 0x0501,
        TurnLaserFalse = 0x0502,
        LaserStatusRequest = 0x0503,
        SetLeftLightBright = 0x0308,
        GetLeftLightBright = 0x030a,
        GetRightLightBright = 0x030b,
        SetRightLightBright = 0x0309,
        GetGasIndicatorDigital = 0x0402,
        GetGasIndicatorAnalog = 0x0401,
        CameraBackPositionRequest = 0x043e06,
        CameraFrontPostitionRequest = 0x043e05,
        CameraBackCalibrationRequest = 0x043c06,
        CameraFrontCalibrationRequest = 0x043c05,
        CameraBackChangePosition = 0x043d06,
        CameraFrontChangePosition = 0x043d05,
    }
    /// <summary>
    /// Commands for robot movings and some settings getting
    /// </summary>
    public enum AuxiliaryCommandsCode {
        TestRequest = 0x01,
        Ping = 0x07,
    }
}
