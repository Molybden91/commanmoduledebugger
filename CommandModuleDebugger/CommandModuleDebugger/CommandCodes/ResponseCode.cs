﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandModuleDebugger
{
    public enum ResponsePredefinedId {
        CrossAnswerID = 0x610,
        FrontCameraAnswerID = 0x151,
        BackCameraAnswerID = 0x153,
        RightFrontWheelAnswerID = 0x600,
        LeftBackWheelAnswerID = 0x601,
        LeftFrontWheelAnswerID = 0x602,
        RightBackWheelAnswerID = 0x603,
        FrontMagnetDriveAnswerID = 0x604,
        BackMagnetDriveAnswerID = 0x605,
        FrontCameraTurnAnswerID = 0x608,
        BackCameraTurnAnswerID = 0x609,
        FrontLeftPhaseArrayDriveAnswerID = 0x60c,
        BackLeftPhaseArrayDriveAnswerID = 0x60d,
        FrontRightPhaseArrayDriveAnswerID = 0x60e,
        BackRightPhaseArrayDriveAnswerID = 0x60f
    }
    /// <summary>
    /// Dlc + data[0]
    /// </summary>
    public enum CrossAnswerCode {
        PlateStarted = 0x0300,
        ButteryVoltageCurrent = 0x0601,
        StopByMagneticField = 0x0102,
        StepFinished = 0x0402,
        InclinometerAcceleration = 0x0704,
        InclinometerAngle = 0x0305,
        MagneticField = 0x0308,
    }
    /// <summary>
    /// Dlc + data[0] + data[1]
    /// </summary>
    public enum CameraAnswerCode {
        ModuleExistence = 0x02010100,
        LeftHeadLightOn = 0x02030400,
        RightHeadLightOn = 0x02030500,
        LeftHeadLightOff = 0x02030600,
        RightHeadLightOff= 0x02030700,
        SetLeftHeadLightBrihtness = 0x04030800,
        GetLeftHeadLightBrihtness = 0x04030A00,
        SetRightHeadLightBrihtness = 0x04030900,
        GetRightHeadLightBrihtness = 0x04030B00,
        GasIndicatorAnalog = 0x03040100,
        GasIndicatorDigital = 0x06040200,
        LaserOn = 0x02050100,
        LaserOff = 0x02050200,
        LaserStatusOn = 0x02050300,
        laserStatusOff = 0x02050400,
        UnknownCommand = 0x04FAFA00,
    }
    /// <summary>
    /// Dlc + data[0]
    /// </summary>
    public enum UniAnswerCode {
        PlateStarted = 0x0300,
        INAProtectionState = 0x0201,
        INACurrent = 0x0302,
        ShuntCurrent = 0x0303,
        SoftCurrentProtectionState = 0x0204,
        ActuatorStopped = 0x0405,
        EndCameraCallibration = 0x0106,
        CameraOnPosition = 0x0207,
        MagneticFieldValue = 0x0308,
        MagneticStateTurned = 0x0209,
        MagneticStateTurnedOff = 0x0209,
        TraveledDistance = 0x050a, //Пройденный путь мотора (в импульсах с датчика холла)
        WheelStepState = 0x040b, //Состояние шага мотор - колеса
        ActuatorInProgress = 0x010c,
        GeneralCurrentProtectionResponse = 0x020d,
        WheelInclinometerAcceleration = 0x070e,
        CentralFlexure = 0x050f,
        MaxFrontMotorDeviation = 0x0510,
        SHIMMotorDeviationMaintaining = 0x0511, //ШИМ моторам для поддержания изгиба
        ImpulsesCountFromHall = 0x0312, //Число импульсов с датчика Холла мотора колес
        MaxbackMotorDeviation = 0x0513,
        StepEnd = 0x0314,
        ActuatorPassedDistance = 0x0415, //Актуатор прошел Х мм и флаг его состояния
    }
    /// <summary>
    /// 
    /// </summary>
}
