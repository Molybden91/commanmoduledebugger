﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace CommandModuleDebugger
{
    public class CANPack
    {
        #region constants
        public static readonly byte[] PackHeader = {0, 0,0x01,0x55 };
        private const int MaxPackLenghtConst = 64;
        #endregion

        #region private fields
        private int MaxLength = MaxPackLenghtConst;
        public List<CanFrame> commands = new List<CanFrame>();
        #endregion

        #region public fields
        public bool isFull;
        #endregion

        #region Constructors
        public CANPack() {
            isFull = false;
        }

        public CANPack(byte[] commandsBuffer) {
            this.commands = fromBuffer(commandsBuffer).commands;
            if (commands.Count >= MaxLength)
            {
                this.isFull = true;
            }
            else {
                this.isFull = false;
            }
        }

        public CANPack(List<CanFrame> commands) {           
            this.commands = commands;
            if (commands.Count >= MaxLength)
            {
                this.isFull = true;
            }
            else {
                this.isFull = false;
            }
        }
        #endregion

        #region public methods
        public byte [] toBuffer() {
            byte[] result = new byte[0];
            if (0 != commands.Count)
            {
                int oneFrameSize = Marshal.SizeOf<CanFrame>();
                result = new byte[oneFrameSize*commands.Count];
                for (int i = 0; i < commands.Count; i++) {
                    byte[] frame = GetBytesFromCanFrame(commands[i]);
                    for (int j = 0; j < oneFrameSize; j++) {
                        result[j + i * oneFrameSize] = frame[j];
                    }
                }
            }
            return result;
        }

        public byte[] PrepareForSending() {
            byte[] forSending = new byte[1028];
            byte[] buffer = this.toBuffer();
            forSending[0] = 0;
            forSending[1] = 0;
            forSending[2] = 0x01;
            forSending[3] = 0x55;
            for (int i = 0; i < buffer.Length; i++)
            {
                forSending[i + 4] = buffer[i];
            }
            return forSending;
        }

        public static CANPack fromBuffer(byte[] commandsBuffer) {
            List<CanFrame> commands = new List<CanFrame>();
            int oneFrameSize = Marshal.SizeOf<CanFrame>();
            byte[] frame = new byte[oneFrameSize];
            int j = 0;
            for (int i = 0; i < commandsBuffer.Length; i++)
            {
                if (j == 16) {
                    CanFrame canFrame = GetCanFrameFromBuffer(frame);
                    if (canFrame.dlc > 0 && canFrame.id > 0)
                    {
                        commands.Add(canFrame);
                    }
                    j = 0;
                    frame = new byte[oneFrameSize];
                }
                frame[j] = commandsBuffer[i];
                j++;
            }            

            CANPack result = new CANPack(commands);
            return result;
        }
        public bool Push(CanFrame frameToPush) {
            if (!isFull) {
                commands.Add(frameToPush);
                if (commands.Count == this.MaxLength) {
                    isFull = true;
                }
                return true;
            }
            return false;
        }

        public static byte[] GetBytesFromCanFrame(CanFrame canFrame)
        {
            int size = Marshal.SizeOf(canFrame);
            byte[] outputArray = new byte[size];
            IntPtr pointer = Marshal.AllocHGlobal(size);
            Marshal.StructureToPtr(canFrame, pointer, true);
            Marshal.Copy(pointer, outputArray, 0, size);
            Marshal.FreeHGlobal(pointer);
            return outputArray;
        }

        public static CanFrame GetCanFrameFromBuffer(byte [] buffer) {
            if (buffer.Length == 16)
            {
                CanFrame result = new CanFrame();
                result.id = 0;
                result.dlc = 0;
                result.reserved = new byte[3];
                result.data = new byte[8];
                result.id = BitConverter.ToInt32(new byte []{ buffer[0], buffer[1], buffer[2], buffer[3] },0);
                result.dlc = buffer[4];
                Buffer.BlockCopy(buffer, 5, result.reserved, 0, result.reserved.Length);
                Buffer.BlockCopy(buffer, 8, result.data, 0, result.data.Length);
                return result;
            }
            else throw new Exception("Unable to convert to CanFrame. The length must be 16bytes");
        }
        #endregion
    }
}
