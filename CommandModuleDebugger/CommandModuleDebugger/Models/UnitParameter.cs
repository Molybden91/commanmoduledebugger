﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandModuleDebugger
{
    public class UnitParameter
    {
        public string UnitName { get; set; }
        public string ParameterName { get; set; }
        public string ParameterValue { get; set; }
        public UnitParameter(string unitName, string parameterName, string parameterValue) {
            this.UnitName = unitName;
            this.ParameterName = parameterName;
            this.ParameterValue = parameterValue;
        }
    }
}
