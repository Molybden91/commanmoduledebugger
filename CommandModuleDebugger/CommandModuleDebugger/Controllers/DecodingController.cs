﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommandModuleDebugger
{
    public class DecodingController
    {
        #region public fields

        #endregion

        #region private fields
        public Dictionary<string, string> GeneralParameters { get; set; }
        public Dictionary<string, Dictionary<string,string>> UnitParameters { get; set; }
        //public List<UnitParameter> UnitParameters { get; set; }
        private CANPack Input_Pack { get; set; }
        #endregion

        #region Constructors
        public DecodingController(CANPack inputPack) {
            GeneralParameters = new Dictionary<string, string>();
            UnitParameters = new Dictionary<string, Dictionary<string, string>>();
            Input_Pack = inputPack;
            ObtainParametersFromDecoded();
        }
        #endregion

        #region public methods
        public void ClearDecodedData() {
            GeneralParameters = new Dictionary<string, string>();
            UnitParameters = new Dictionary<string, Dictionary<string, string>>();
        }
        public void SetInput_Pack(CANPack Newpack){
            this.Input_Pack = Newpack;
            ObtainParametersFromDecoded();
        }
        public Dictionary<string, string> GetGeneralParameters() {
            return GeneralParameters;
        }
        public Dictionary<string, Dictionary<string, string>> GetUnitParameters() {
            return UnitParameters;
        }
        #endregion

        #region private methods
        private void ObtainParametersFromDecoded()
        {
            Dictionary<ulong, CanFrame> DecodedInput = DecodeInformation();

            CanFrame currentFrame;
            foreach (KeyValuePair<ulong, CanFrame> pair in DecodedInput)
            {
                currentFrame = pair.Value;
                switch (currentFrame.id)
                {
                    case (int)ResponsePredefinedId.CrossAnswerID:
                        GetCrossAnswer(currentFrame);
                        break;
                    case (int)ResponsePredefinedId.FrontCameraAnswerID:
                        GetCameraAnswer(currentFrame, "Передняя камера.");
                        break;
                    case (int)ResponsePredefinedId.BackCameraAnswerID:
                        GetCameraAnswer(currentFrame, "Задняя камера.");
                        break;
                    case (int)ResponsePredefinedId.RightFrontWheelAnswerID:
                        GetUniAnswer(currentFrame, "Правое переднее колесо.");
                        break;
                    case (int)ResponsePredefinedId.LeftBackWheelAnswerID:
                        GetUniAnswer(currentFrame, "Левое заднее колесо.");
                        break;
                    case (int)ResponsePredefinedId.LeftFrontWheelAnswerID:
                        GetUniAnswer(currentFrame, "Левое переднее колесо.");
                        break;
                    case (int)ResponsePredefinedId.RightBackWheelAnswerID:
                        GetUniAnswer(currentFrame, "Правое заднее колесо.");
                        break;
                    case (int)ResponsePredefinedId.FrontMagnetDriveAnswerID:
                        GetUniAnswer(currentFrame, "Передний привод магнита.");
                        break;
                    case (int)ResponsePredefinedId.BackMagnetDriveAnswerID:
                        GetUniAnswer(currentFrame, "Задний привод магнита.");
                        break;
                    case (int)ResponsePredefinedId.FrontCameraTurnAnswerID:
                        GetUniAnswer(currentFrame, "Привод поворота переденй камеры.");
                        break;
                    case (int)ResponsePredefinedId.BackCameraTurnAnswerID:
                        GetUniAnswer(currentFrame, "Привод поворота задней камеры.");
                        break;
                    case (int)ResponsePredefinedId.FrontLeftPhaseArrayDriveAnswerID:
                        GetUniAnswer(currentFrame, "Передний левый привод решетки.");
                        break;
                    case (int)ResponsePredefinedId.BackLeftPhaseArrayDriveAnswerID:
                        GetUniAnswer(currentFrame, "Задний левый привод решетки.");
                        break;
                    case (int)ResponsePredefinedId.FrontRightPhaseArrayDriveAnswerID:
                        GetUniAnswer(currentFrame, "Передний правый привод решетки.");
                        break;
                    case (int)ResponsePredefinedId.BackRightPhaseArrayDriveAnswerID:
                        GetUniAnswer(currentFrame, "Задний правый привод решетки.");
                        break;
                }
            }
        }
        private Dictionary<ulong, CanFrame> DecodeInformation()
        {
            Dictionary<ulong, CanFrame> ValuableObtainedInformation = new Dictionary<ulong, CanFrame>();

            byte[] key;
            List<CanFrame> inputFrames = Input_Pack.commands;
            for (int i = 0; i < inputFrames.Count; i++)
            {
                byte[] id = BitConverter.GetBytes(inputFrames[i].id);
                key = new byte[] { id[0], id[1], id[2], id[3], inputFrames[i].dlc, inputFrames[i].data[0], 0, 0 };
                ulong keyValue = BitConverter.ToUInt64(key, 0);
                if (keyValue > 0)
                {
                    if (ValuableObtainedInformation.ContainsKey(keyValue))
                    {
                        ValuableObtainedInformation[keyValue] = inputFrames[i];
                    }
                    else
                    {
                        ValuableObtainedInformation.Add(keyValue, inputFrames[i]);
                    }
                }
            }
            return ValuableObtainedInformation;
        }

        private void GetCrossAnswer(CanFrame currentFrame) {
            byte[] identifier = new byte[] { currentFrame.data[0], currentFrame.dlc};
            short commandIdentifier = BitConverter.ToInt16(identifier,0);
            switch (commandIdentifier) {
                case (int)CrossAnswerCode.ButteryVoltageCurrent:
                    GeneralParameters.Add("Батарея:", ((short)(currentFrame.data[2] + (short)(currentFrame.data[1] << 8))).ToString());
                    GeneralParameters.Add("Ток:", ((short)(currentFrame.data[4] + (short)(currentFrame.data[3] << 8))).ToString());
                    GeneralParameters.Add("Число светодиодов:", currentFrame.data[5].ToString());
                    break;
                case (int)CrossAnswerCode.InclinometerAcceleration:
                    GeneralParameters.Add("gx:", ((short)(currentFrame.data[2] + (short)(currentFrame.data[1] << 8))).ToString());
                    GeneralParameters.Add("gy:", ((short)(currentFrame.data[4] + (short)(currentFrame.data[3] << 8))).ToString());
                    GeneralParameters.Add("gz:", ((short)(currentFrame.data[6] + (short)(currentFrame.data[5] << 8))).ToString());
                    break;
                case (int)CrossAnswerCode.InclinometerAngle:
                    GeneralParameters.Add("Угол удержания инклинометра:", ((short)(currentFrame.data[2] + (short)(currentFrame.data[1] << 8))).ToString());
                    break;
                case (int)CrossAnswerCode.PlateStarted:
                    GeneralParameters.Add("Version1:", currentFrame.data[1].ToString());
                    GeneralParameters.Add("Version2:", currentFrame.data[2].ToString());
                    break;
                case (int)CrossAnswerCode.StepFinished:
                    GeneralParameters.Add("Шаг:", ((short)(currentFrame.data[3] + (short)(currentFrame.data[2] << 8))).ToString());
                    break;
                case (int)CrossAnswerCode.StopByMagneticField:
                    GeneralParameters.Add("Останов по магнитному полю:", "Выполнен");
                    break;
                case (int)CrossAnswerCode.MagneticField:
                    GeneralParameters.Add("Магнитное поле:", ((short)(currentFrame.data[2] + (short)(currentFrame.data[1] << 8))).ToString());
                    break;
            }
        }
        private void GetCameraAnswer(CanFrame currentFrame, string camera) {
            byte[] identifier = new byte[] { 0,currentFrame.data[1],currentFrame.data[0], currentFrame.dlc };
            Dictionary<string, string> inner = new Dictionary<string, string>();
            int commandIdentifier = BitConverter.ToInt32(identifier, 0);
            string Parameter = "";
            string Value = "";
            switch (commandIdentifier) {
                case (int)CameraAnswerCode.GasIndicatorAnalog:
                    Parameter = "Загазованность:";
                    Value = currentFrame.data[3].ToString();
                    break;
                case (int)CameraAnswerCode.GasIndicatorDigital:
                    Parameter = "Загазованность:";
                    Value = BitConverter.ToSingle(new byte[] { currentFrame.data[3], currentFrame.data[4], currentFrame.data[5], currentFrame.data[6] }, 0).ToString();
                    break;
                case (int)CameraAnswerCode.GetLeftHeadLightBrihtness:
                    Parameter = "Яркость левой фары:";
                    Value = ((short)(currentFrame.data[4] + (short)(currentFrame.data[3] << 8))).ToString();
                    break;
                case (int)CameraAnswerCode.GetRightHeadLightBrihtness:
                    Parameter = "Яркость правой фары:";
                    Value = ((short)(currentFrame.data[4] + (short)(currentFrame.data[3] << 8))).ToString();
                    break;
                case (int)CameraAnswerCode.LaserOff:
                    Parameter = "Лазер:";
                    Value = "Выключение произведено";
                    break;
                case (int)CameraAnswerCode.LaserOn:
                    Parameter = "Лазер:";
                    Value = "Включение произведено";
                    break;
                case (int)CameraAnswerCode.laserStatusOff:
                    Parameter = "Лазер:";
                    Value = "Выключен";
                    break;
                case (int)CameraAnswerCode.LaserStatusOn:
                    Parameter = "Лазер:";
                    Value = "Включен";
                    break;
                case (int)CameraAnswerCode.LeftHeadLightOff:
                    Parameter = "Левая фара:";
                    Value = "Выключена";
                    break;
                case (int)CameraAnswerCode.LeftHeadLightOn:
                    Parameter = "Левая фара:";
                    Value = "Включена";
                    break;
                case (int)CameraAnswerCode.ModuleExistence:
                    Parameter = "Модуль:";
                    Value = "Наличествует";
                    break;
                case (int)CameraAnswerCode.RightHeadLightOff:
                    Parameter = "Правая фара:";
                    Value = "Выключена";
                    break;
                case (int)CameraAnswerCode.RightHeadLightOn:
                    Parameter = "Правая фара:";
                    Value = "Включена";
                    break;
                case (int)CameraAnswerCode.SetLeftHeadLightBrihtness:
                    Parameter = "Яркость левой фары установлена:";
                    Value = ((short)(currentFrame.data[4] + (short)(currentFrame.data[3] << 8))).ToString();
                    break;
                case (int)CameraAnswerCode.SetRightHeadLightBrihtness:
                    Parameter = "Яркость правой фары установлена:";
                    Value = ((short)(currentFrame.data[4] + (short)(currentFrame.data[3] << 8))).ToString();
                    break;
                case (int)CameraAnswerCode.UnknownCommand:
                    Parameter = "Команда:";
                    Value = "не распознана";
                    break;
            }
            if (UnitParameters.ContainsKey(camera))
            {
                UnitParameters[camera].Add(Parameter, Value);
            }
            else
            {
                inner.Add(Parameter, Value);
                UnitParameters.Add(camera, inner);
            }
        }
        private void GetUniAnswer(CanFrame currentFrame, string module) {
            byte[] identifier = new byte[] { currentFrame.data[0], currentFrame.dlc };
            Dictionary<string, string> inner = new Dictionary<string, string>();
            short commandIdentifier = BitConverter.ToInt16(identifier, 0);
            string Parameter1 = "";
            string Value1 = "";
            string Parameter2 = "";
            string Value2 = "";
            string Parameter3 = "";
            string Value3 = "";
            switch (commandIdentifier) {
                case (int)UniAnswerCode.ActuatorInProgress:
                    Parameter1 = "Актуатор выполняет предыдущую команду";
                    Value1 = "";
                    break;
                case (int)UniAnswerCode.ActuatorPassedDistance:
                    Parameter1 = "Актуатор прошел:";
                    Value1 = ((short)(currentFrame.data[2] + (short)(currentFrame.data[1] << 8))).ToString();
                    Parameter2 = "Флаг состояния актуатора:";
                    Value2 = currentFrame.data[3].ToString();
                    break;
                case (int)UniAnswerCode.ActuatorStopped:
                    Parameter1 = "Актуатор остановился, положение:";
                    Value1 = currentFrame.data[1].ToString();
                    Parameter2 = "Актуатор остановился, путь:";
                    Value2 = ((short)(currentFrame.data[3] + (short)(currentFrame.data[2] << 8))).ToString();
                    break;
                case (int)UniAnswerCode.CameraOnPosition:
                    Parameter1 = "Камера заняла положение:";
                    Value1 = currentFrame.data[1].ToString();
                    break;
                case (int)UniAnswerCode.CentralFlexure:
                    Parameter1 = "Изгиб центральной части. Канал1:";
                    Value1 = ((short)(currentFrame.data[2] + (short)(currentFrame.data[1] << 8))).ToString();
                    Parameter2 = "Изгиб центральной части. Канал2:";
                    Value2 = ((short)(currentFrame.data[4] + (short)(currentFrame.data[3] << 8))).ToString(); ;
                    break;
                case (int)UniAnswerCode.EndCameraCallibration:
                    Parameter1 = "Камера закончила калибровку";
                    Value1 = "";
                    break;
                case (int)UniAnswerCode.GeneralCurrentProtectionResponse:
                    Parameter1 = "Общий ответ о состоянии токовых защит:";
                    Value1 = currentFrame.data[1].ToString();
                    break;
                case (int)UniAnswerCode.ImpulsesCountFromHall:
                    Parameter1 = "Число импульсов с датчика Холла мотора колес:";
                    Value1 = ((short)(currentFrame.data[2] + (short)(currentFrame.data[1] << 8))).ToString();
                    break;
                case (int)UniAnswerCode.INACurrent:
                    Parameter1 = "Значение тока с INA:";
                   Value1 = ((short)(currentFrame.data[2] + (short)(currentFrame.data[1] << 8))).ToString();
                    //GeneralParameters.Add(Parameter1,Value1);
                    break;
                case (int)UniAnswerCode.INAProtectionState:
                    Parameter1 = "Состояние защиты INA:";
                    Value1 = currentFrame.data[1].ToString();
                    //GeneralParameters.Add(Parameter1, Value1);
                    break;
                case (int)UniAnswerCode.MagneticFieldValue:
                    Parameter1 = "Уровень магнитного поля:";
                    Value1 = ((short)(currentFrame.data[2] + (short)(currentFrame.data[1] << 8))).ToString();
                    break;
                case (int)UniAnswerCode.MagneticStateTurned:
                    Parameter1 = "Состояние магнита переключено:";
                    Value1 = currentFrame.data[1].ToString();
                    break;
                case (int)UniAnswerCode.MaxbackMotorDeviation:
                    Parameter1 = "Предельные отклонения заднего мотора, левый:";
                    Value1 = ((short)(currentFrame.data[2] + (short)(currentFrame.data[1] << 8))).ToString();
                    Parameter2 = "Предельные отклонения заднего мотора, правый:";
                    Value2 = ((short)(currentFrame.data[4] + (short)(currentFrame.data[3] << 8))).ToString();
                    break;
                case (int)UniAnswerCode.MaxFrontMotorDeviation:
                    Parameter1 = "Предельные отклонения переднего мотора, левый:";
                    Value1 = ((short)(currentFrame.data[2] + (short)(currentFrame.data[1] << 8))).ToString();
                    Parameter2 = "Предельные отклонения переднего мотора, правый:";
                    Value2 = ((short)(currentFrame.data[4] + (short)(currentFrame.data[3] << 8))).ToString();
                    break;
                case (int)UniAnswerCode.PlateStarted:
                    Parameter1 = "Плата запустилась, Version1:";
                    Value1 = currentFrame.data[1].ToString();
                    Parameter2 = "Плата запустилась, Version2:";
                    Value2 = currentFrame.data[2].ToString();
                    break;
                case (int)UniAnswerCode.SHIMMotorDeviationMaintaining:
                    Parameter1 = "ШИМ моторам для поддержания изгиба Vlf:";
                    Value1 = currentFrame.data[1].ToString();
                    Parameter2 = "ШИМ моторам для поддержания изгиба Vrf:";
                    Value2 = currentFrame.data[2].ToString();
                    Parameter3 = "ШИМ моторам для поддержания изгиба Vrf:";
                    Value3 = currentFrame.data[3].ToString();
                    inner.Add("ШИМ моторам для поддержания изгиба Vrb:", currentFrame.data[1].ToString());
                    break;
                case (int)UniAnswerCode.ShuntCurrent:
                    Parameter1 = "Значение тока с шунта:";
                    Value1 = ((short)(currentFrame.data[2] + (short)(currentFrame.data[1] << 8))).ToString();
                    break;
                case (int)UniAnswerCode.SoftCurrentProtectionState:
                    Parameter1 = "Состояние софт-защиты по току:";
                    Value1 = ((short)(currentFrame.data[2] + (short)(currentFrame.data[1] << 8))).ToString();
                    break;
                case (int)UniAnswerCode.StepEnd:
                    Parameter1 = "Окончание шага. Шаг:";
                    Value1 = ((short)(currentFrame.data[2] + (short)(currentFrame.data[1] << 8))).ToString();
                    break;
                case (int)UniAnswerCode.TraveledDistance:
                    Parameter1 = "Пройденный путь мотора:";
                    Value1 = (currentFrame.data[4] + (currentFrame.data[3] << 8) + (currentFrame.data[1] << 16) + (currentFrame.data[1] << 24)).ToString();
                    break;
                case (int)UniAnswerCode.WheelInclinometerAcceleration:
                    Parameter1 = "Ускорение инклинометра колеса gx:";
                    Value1 = ((short)(currentFrame.data[2] + (short)(currentFrame.data[1] << 8))).ToString();
                    Parameter2 = "Ускорение инклинометра колеса gy:";
                    Value2 = ((short)(currentFrame.data[4] + (short)(currentFrame.data[3] << 8))).ToString();
                    Parameter2 = "Ускорение инклинометра колеса gz:";
                    Value2 = ((short)(currentFrame.data[6] + (short)(currentFrame.data[5] << 8))).ToString();
                    break;
                case (int)UniAnswerCode.WheelStepState:
                    Parameter1 = "Состояние шага мотор-колеса:";
                    Value1 = ((short)(currentFrame.data[2] + (short)(currentFrame.data[1] << 8))).ToString();
                    Parameter2 = "";
                    Value2 = currentFrame.data[3].ToString();
                    break;
            }
            if (UnitParameters.ContainsKey(module))
            {
                UnitParameters[module].Add(Parameter1, Value1);
                if (!Parameter2.Equals("")) UnitParameters[module].Add(Parameter2, Value2);
                if (!Parameter3.Equals("")) UnitParameters[module].Add(Parameter3, Value3);
            }
            else
            {
                inner.Add(Parameter1, Value1);
                if (!Parameter2.Equals("")) inner.Add(Parameter2, Value2);
                if (!Parameter3.Equals("")) inner.Add(Parameter3, Value3);
                UnitParameters.Add(module, inner);
            }
        }
        #endregion

    }
}
