﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CommandModuleDebugger
{
    class CommandControllerUnit : CommandController
    {
        #region public methods
        public CANPack SendCommandPack(EthernetDevice device)
        {
            return PerformInteraction(device,500);
        }

        public CANPack SendCommandPack(EthernetDevice device, int WaitingTime ) {
            return PerformInteraction(device, WaitingTime);
        }

        public void DeleteLastEnteredCommand()
        {
            CanFrame lastElement = Output_Pack.commands.Last();
            Output_Pack.commands.Remove(lastElement);
        }

        public void AddCommandToPack(long commandCode, int commandID)
        {
            Output_Pack.Push(CreateCommand(commandCode, commandID));
        }
        public void AddPreparedFrameToPack(CanFrame frame) {
            Output_Pack.Push(frame);
        }
        public void AddParameterizedCommandToPack(long commandCode, int commandID, byte insertOffset, byte[] parameters) {
            Output_Pack.Push(CreateParametrizedCommand(commandCode,commandID, insertOffset, parameters));
        }
        #endregion
        protected CanFrame CreateParametrizedCommand(long commandCode, int commandID, byte insertOffset, byte[] parameters) {
            if (parameters.Length + insertOffset < 8)
            {
                CanFrame preparedFrame = CreateCommand(commandCode, commandID);
                byte[] data = preparedFrame.data;
                for (int i = 0; i < parameters.Length; i++)
                {
                    data[i + insertOffset] = parameters[i];
                }
                preparedFrame.data = data;
                preparedFrame.dlc = (byte)(preparedFrame.dlc + (byte)parameters.Length);
                return preparedFrame;
            }
            else {
                throw new ArgumentOutOfRangeException("Check the amount of data to insert first");
            }
        }
    }
}
