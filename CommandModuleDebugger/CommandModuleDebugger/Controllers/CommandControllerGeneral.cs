﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CommandModuleDebugger
{
    public class CommandControllerGeneral: CommandController
    {
        #region public methods
        public CANPack PerformGeneralCheck(EthernetDevice device)
        {
            Output_Pack.Push(CreateCommand((long)AuxiliaryCommandsCode.Ping, (int)CommandPreDefinedId.CAN_IDCrossCmd));
            return PerformInteraction(device,500);
        }
        #endregion
    }
}
