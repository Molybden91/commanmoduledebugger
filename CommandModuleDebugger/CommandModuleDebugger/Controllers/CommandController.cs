﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace CommandModuleDebugger
{
    public class CommandController
    {
        #region Constants
        private const int ReceivingBufferSizeConstant = 1446;
        #endregion
        #region protected fields
        protected CANPack Output_Pack;
        #endregion

        #region Constructors
        public CommandController() {
            Output_Pack = new CANPack();
        }
        #endregion

        #region public methods
        public void ClearCommandPack() {
            Output_Pack = new CANPack();
        }
        #endregion

        #region protected methods
        protected CanFrame CreateCommand(long commandCode, int commandID) {
            CanFrame command = new CanFrame();
            command.id = commandID;
            command.reserved = new byte[3];
            command.data = new byte[8];
            byte[] tmp = new byte[8];
            byte valuableDataCount = 0;
            for (int i = 0; i < 8; i++)
            {
                tmp[i] = (byte)((commandCode >> (8 * i)) & 0xFF);
                if (tmp[i] != 0) {
                    valuableDataCount = (byte)(i+1);
                }
            }
            for (int j = 0; j < valuableDataCount; j++) {
                command.data[valuableDataCount - j - 1] = tmp[j]; 
            }
            command.dlc = valuableDataCount;
            return command;
        }
        protected CANPack PerformInteraction(EthernetDevice device, int receiveWaitingTimeout) {
            if (device != null)
            {
                device.ConnectDevice();
                byte[] forSending = Output_Pack.PrepareForSending();
                device.SendData(forSending, forSending.Length);

                byte[] obtainedData = new byte[ReceivingBufferSizeConstant];
                Thread.Sleep(receiveWaitingTimeout);
                device.ReceiveData(obtainedData);
                /**
                if (obtainedData.Length == 1446) {
                    WriteToFile(obtainedData);
                }
                **/
                device.DisconnectDevice();
                return CANPack.fromBuffer(obtainedData);
            }
            else {
                MessageBox.Show("Unable to perform action. Try to connect to device first");
                return new CANPack();
            }
        }
        protected void WriteToFile(byte [] buffer) {
            /**
            Microsoft.Win32.SaveFileDialog dialog = new Microsoft.Win32.SaveFileDialog()
            {
                DefaultExt = ".txt",
                Filter = "text file(*.txt)|*.txt",
                AddExtension = true,
                FileName = "Export_FromData_File_" + Convert.ToString(DateTime.Now)
            };
            if (dialog.ShowDialog() == true)
    **/
            {
                try
                {
                    FileStream fileStream = new FileStream(@"C:\Users\am\Documents\test1.txt", FileMode.Append, FileAccess.Write);
                    StreamWriter fileWriter = new StreamWriter(fileStream);
                    for (int i = 0; i < buffer.Length; i++)
                    {
                        fileWriter.WriteLine(buffer[i]);
                    }
                    fileWriter.Flush();
                    fileWriter.Close();
                    fileStream.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "Error",
                           MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        #endregion

    }
}
